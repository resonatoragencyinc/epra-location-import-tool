=== EPRA Location Import Tool ===
Contributors: resonator
Donate link: https://resonator.ca
Requires at least: 5.2.2
Tested up to: 5.2.2
Stable tag: trunk
Requires PHP: 7.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Manage the one-way sync of locations from EPRA’s location database to the RME sites.

== Description ==

Manage the one-way sync of locations from EPRA’s location database to the RME sites.

== Installation ==

1. Upload `epra-location-import-tool.php` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Use the Network Admin->Location Tool screen to configure the plugin

== Frequently Asked Questions ==

= Based on Count of rows =

Query should return zero (0). A negative result indicates there are more locations in WP then in the origin table. A positive result indicates there are more locations in the origin table then in WP.
`SELECT`
`    (`
`        SELECT`
`            COUNT(*)`
`        FROM`
`            {origin table} loc`
`        WHERE`
`            loc.sl_latitude IS NOT NULL`
`            AND loc.sl_latitude != '') - (`
`            SELECT`
`                COUNT(*)`
`            FROM`
`                wp_{blog_id}_posts p`
`            WHERE`
`                p.post_type = 'wpsl_stores') AS C;`

= Check via unique store ID (if available) =

Query will return any IDs that do not exist in both tables. This query relies on each store having a unique ID and that ID being in the post_meta table.
`SELECT`
`    ID,`
`    COUNT(ID) AS C`
`FROM (`
`    SELECT`
`        pm.meta_value AS ID`
`    FROM`
`        wp_{blog_id}_postmeta pm`
`    LEFT JOIN wp_{blog_id}_posts p ON p.ID = pm.post_id`
`WHERE`
`    pm.meta_key = 'store_id'`
`    AND p.post_type = 'wpsl_stores'`
`UNION ALL`
`SELECT`
`    loc.sl_id AS ID`
`FROM`
`    {origin table} loc`
`WHERE`
`    loc.sl_latitude IS NOT NULL`
`    AND loc.sl_latitude != '') AS T`
`GROUP BY`
`    ID`
`HAVING`
`    C < 2;`

== Screenshots ==

1. Darwin

== Changelog ==

= 1.0.8 =
* added in check for sl_url when checking for differences

= 1.0.7 =
* Updated help text, spelling errors.
* Removed (broken) AJAX check for CSV download.
* Extra blank field checks to remove null errors.

= 1.0.4 =
* Added a whole bunch of hooks. See documentation for more information.
* Commented all functions and classes.

= 1.0.2 =
* Bunch of validation checks

= 1.0.1 =
* Prettier admin page
* Better sanitization
* More hooks
* Updater now checks server instead of git

= 1.0.0 =
* First Release

== Upgrade Notice ==

= 1.0.4 =
New hooks are available!

= 1.0.1 =
Prettier admin page, fixed updater

= 1.0.0 =
First Release
