<?php
/**
 * Base plugin file.
 *
 * @since 1.0.0
 * @package Epra_Location_Import_Tool
 *
 * @wordpress-plugin
 * Plugin Name: EPRA Location Import Tool
 * Description: Manage the one-way sync of locations from EPRA’s location database to the RME sites.
 * Version:     1.0.8
 * Author:      Resonator Agency Inc.
 * Author URI:  https://resonator.ca
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain: epra-location-import-tool
 * Domain Path: /languages
 */

if ( ! defined( 'ABSPATH' ) ) {
	die;
}

// Autoload composer.
require_once plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';

use \Epra_Location_Import_Tool\includes\Activator;
use \Epra_Location_Import_Tool\includes\Deactivator;
use \Epra_Location_Import_Tool\Base;

define( 'ELIT', 'epra-location-import-tool' );
define( 'ELIT_VERSION', '1.0.8' );
define( 'ELIT_SLUG', 'epra_location_import_tool' );
define( 'ELIT_BASE_DIR', plugin_dir_path( __FILE__ ) );
define( 'ELIT_SRC_DIR', plugin_dir_path( __FILE__ ) . 'src/' );
define( 'ELIT_BASE_NAME', plugin_basename( __FILE__ ) );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-activator.php
 *
 * @since 1.0.0
 *
 * @internal Only to be used by activation hook.
 *
 * @return void
 */
function activate_epra_location_import_tool() {
	Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-deactivator.php
 *
 * @since 1.0.0
 *
 * @internal Only to be used by deactivation hook.
 *
 * @return void
 */
function deactivate_epra_location_import_tool() {
	Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_epra_location_import_tool' );
register_deactivation_hook( __FILE__, 'deactivate_epra_location_import_tool' );

if ( is_admin() ) {
	/**
	 * A custom update checker for WordPress plugins.

	 * @link http://w-shadow.com/blog/2011/06/02/automatic-updates-for-commercial-themes/
	 * @link https://github.com/YahnisElsts/plugin-update-checker
	 * @link https://github.com/YahnisElsts/wp-update-server
	 */
	$my_update_checker = \Puc_v4_Factory::buildUpdateChecker(
		'https://resonator.ca/wp-update/?action=get_metadata&slug=epra-location-import-tool', // Metadata URL.
		__FILE__, // Full path to the main plugin file.
		ELIT // Plugin slug. Usually it's the same as the name of the directory.
	);

	/**
	 * Add plugin upgrade notification.
	 */
	add_action( 'in_plugin_update_message-' . ELIT . '/' . ELIT . '.php', 'epra_location_import_tool_show_upgrade_notification', 10, 2 );
	/**
	 * Echos the upgrade notification.
	 *
	 * @since 1.0.0
	 * @see https://developer.wordpress.org/reference/hooks/in_plugin_update_message-file/
	 *
	 * @internal
	 *
	 * @param object $current_plugin_metadata An array of plugin metadata.
	 * @param object $new_plugin_metadata     An array of metadata about the available plugin update.
	 *
	 * @return void
	 */
	function epra_location_import_tool_show_upgrade_notification( $current_plugin_metadata, $new_plugin_metadata ) {
		/**
		 * Check "upgrade_notice" in readme.txt.
		 * https://andidittrich.de/2015/05/howto-upgrade-notice-for-wordpress-plugins.html
		 * Eg.:
		 * == Upgrade Notice ==
		 * = 20180624 = <- new version
		 * Notice       <- message
		 */
		if ( isset( $new_plugin_metadata->upgrade_notice ) && strlen( trim( $new_plugin_metadata->upgrade_notice ) ) > 0 ) {
			// Display "upgrade_notice".
			echo sprintf(
				'<span style="background-color:#d54e21;padding:10px;color:#f9f9f9;margin-top:10px;display:block;"><strong>%1$s: </strong>%2$s</span>',
				esc_attr( 'Important Upgrade Notice', 'exopite-multifilter' ),
				esc_html( rtrim( $new_plugin_metadata->upgrade_notice ) )
			);
		}

	}
}

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since 1.0.0
 *
 * @internal Run this bad boy.
 *
 * @return void
 */
function run_epra_location_import_tool() {

	$plugin = new Base();
	$plugin->run();

}
run_epra_location_import_tool();
