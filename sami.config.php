<?php

use Sami\Sami;
use Symfony\Component\Finder\Finder;

$iterator = Finder::create()
	->files()
	->name( '*.php' )
	->exclude( 'vendor' )
	->exclude( 'node_modules' )
	->in( '.' );

return new Sami(
	$iterator,
	array(
		'title'                => 'EPRA Locatio Import Tool API',
		'build_dir'            => __DIR__ . '/build/',
		'cache_dir'            => __DIR__ . '/cache/',
		'default_opened_level' => 2,
	)
);
