<?php
/**
 * The core plugin class.
 *
 * @since 1.0.0
 *
 * @package    Epra_Location_Import_Tool
 * @subpackage Epra_Location_Import_Tool
 */

namespace Epra_Location_Import_Tool;

use \Epra_Location_Import_Tool\includes\Ajax;
use \Epra_Location_Import_Tool\includes\I18n;
use \Epra_Location_Import_Tool\includes\Importer;
use \Epra_Location_Import_Tool\includes\Loader;
use \Epra_Location_Import_Tool\includes\Logger;
use \Epra_Location_Import_Tool\includes\Wpsl;
use \Epra_Location_Import_Tool\admin\Admin;
use \Epra_Location_Import_Tool\frontend\Frontend;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 */
class Base {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since 1.0.0
	 *
	 * @var Loader $loader Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since 1.0.0
	 *
	 * @var string $epra_location_import_tool The string used to uniquely identify this plugin.
	 */
	protected $epra_location_import_tool;

	/**
	 * The current version of the plugin.
	 *
	 * @since 1.0.0
	 *
	 * @var string $version The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		if ( defined( 'ELIT_VERSION' ) ) {
			$this->version = ELIT_VERSION;
		} else {
			$this->version = '1.0.0';
		}

		$this->epra_location_import_tool = ELIT;

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		$this->define_wpsl_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Epra_Location_Import_Tool_Loader. Orchestrates the hooks of the plugin.
	 * - Epra_Location_Import_Tool_i18n. Defines internationalization functionality.
	 * - Epra_Location_Import_Tool_Admin. Defines all hooks for the admin area.
	 * - Epra_Location_Import_Tool_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @return void
	 */
	private function load_dependencies() {

		$this->loader = new Loader();

	} // load_dependencies

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	private function set_locale() {

		$plugin_i18n = new I18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	} // set_locale

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @return void
	 */
	private function define_admin_hooks() {

		$plugin_admin    = new Admin( $this->get_epra_location_import_tool(), $this->get_version() );
		$plugin_ajax     = Ajax::get_instance();
		$plugin_logger   = new Logger();
		$plugin_importer = new Importer();

		// Admin hooks.
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'network_admin_menu', $plugin_admin, 'add_log_submenu' );
		$this->loader->add_action( 'load-location-import_page_epra_location_import_tool_log', $plugin_admin, 'add_log_view_options' );
		$this->loader->add_filter( 'set-screen-option', $plugin_admin, 'set_log_view_options', 10, 3 );
		$this->loader->add_action( 'after_option_update_epra_location_import_tool', $plugin_admin, 'create_compare_cron_jobs' );

		// AJAX hooks.
		$this->loader->add_action( 'wp_ajax_delete_locations', $plugin_ajax, 'delete_locations' );
		$this->loader->add_action( 'wp_ajax_refresh_locations', $plugin_ajax, 'refresh_locations' );
		$this->loader->add_action( 'wp_ajax_export_origin', $plugin_ajax, 'export_origin' );
		$this->loader->add_action( 'wp_ajax_export_wpsl', $plugin_ajax, 'export_wpsl' );
		$this->loader->add_action( 'wp_ajax_view_location_tool_log', $plugin_ajax, 'view_location_tool_log' );

		// Logger hooks.
		$this->loader->add_action( 'init', $plugin_logger, 'register_post_type' );
		$this->loader->add_action( 'init', $plugin_logger, 'register_taxonomy' );
		$this->loader->add_action( 'epra_location_import_tool_logger_prune_routine', $plugin_logger, 'prune_logs' );

		$plugin_admin->add_network_admin_menu();

		$this->loader->add_filter(
			'wp_logging_should_we_prune',
			'anonymous',
			function( $should_we_prune ) {
				return true;
			}
		);

		$this->loader->add_action( 'epra_location_import_hook', $plugin_importer, 'epra_location_import_cron' );

	} // define_admin_hooks

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 *
	 * @internal
	 *
	 * @return void
	 */
	private function define_public_hooks() {

		$plugin_public = new Frontend( $this->get_epra_location_import_tool(), $this->get_version() );

		$this->loader->add_action( 'wpsl_store_meta', $plugin_public, 'strip_restricted_urls_from_meta', 10, 2 );

	} // define_public_hooks

	/**
	 * Register all of the hooks related to the WPSL functionality
	 * of the plugin.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @return void
	 */
	private function define_wpsl_hooks() {

		$plugin_wpsl = new Wpsl();

		$this->loader->add_action( 'wpsl_meta_box_fields', $plugin_wpsl, 'custom_meta_box_fields' );

	} // define_wpsl_hooks

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @return void
	 */
	public function run() {
		$this->loader->run();
	} // run

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since 1.0.0
	 *
	 * @return string The name of the plugin.
	 */
	public function get_epra_location_import_tool() {
		return $this->epra_location_import_tool;
	} // get_epra_location_import_tool

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @return Loader Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	} // get_loader

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since 1.0.0
	 *
	 * @return string The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	} // get_version

	/**
	 * Wrapper to return all the plugin's options.
	 *
	 * @since 1.0.0
	 *
	 * @return array|string
	 */
	public static function get_options() {
		return get_site_option( 'epra_location_import_tool' );
	}

} // Epra_Location_Import_Tool
