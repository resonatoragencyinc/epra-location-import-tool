<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @since 1.0.0
 *
 * @package    Epra_Location_Import_Tool
 * @subpackage Epra_Location_Import_Tool/admin
 */

namespace Epra_Location_Import_Tool\admin;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

use \Epra_Location_Import_Tool\admin\Settings_Framework;
use \Epra_Location_Import_Tool\admin\Logger_List;

/**
 * Defines the admin-specific functionality of the plugin.
 */
class Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since 1.0.0
	 *
	 * @var string $epra_location_import_tool The ID of this plugin.
	 */
	private $epra_location_import_tool;

	/**
	 * The version of this plugin.
	 *
	 * @since 1.0.0
	 *
	 * @var string $version The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since 1.0.0
	 *
	 * @param string $epra_location_import_tool The name of this plugin.
	 * @param string $version                   The version of this plugin.
	 */
	public function __construct( $epra_location_import_tool, $version ) {

		$this->epra_location_import_tool = $epra_location_import_tool;
		$this->version                   = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @return void
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->epra_location_import_tool, plugin_dir_url( __FILE__ ) . 'css/epra-location-import-tool-admin.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'tabby-ui-vertical', plugin_dir_url( __FILE__ ) . 'css/vendor/tabby-ui-vertical.min.css', array(), '12.0.1', 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @return void
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( 'tabby.polyfills', plugin_dir_url( __FILE__ ) . 'js/vendor/tabby.polyfills.min.js', array(), '12.0.1', false );
		wp_enqueue_script( 'micromodal', plugin_dir_url( __FILE__ ) . 'js/vendor/micromodal.min.js', array( 'jquery' ), '0.4.0', true );
		wp_enqueue_script( $this->epra_location_import_tool, plugin_dir_url( __FILE__ ) . 'js/epra-location-import-tool-admin.js', array( 'jquery' ), $this->version, true );

	}

	/**
	 * Menu item will allow us to load the page to display the plugin's option page.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @return void
	 */
	public function add_network_admin_menu() {

		// Create our options page.
		$menu = array(
			'page_title' => __( 'EPRA Location Import Tool', 'epra-location-import-tool' ),
			'menu_title' => __( 'Location Import', 'epra-location-import-tool' ),
			'capability' => 'manage_network',
			'slug'       => 'epra_location_import_tool',
			'icon'       => 'dashicons-admin-site-alt3',
			'template'   => 'partials/admin-display.php',
		);

		$sites = get_sites(
			array(
				'public'            => 1,
				'no_found_rows'     => false,
				'update_site_cache' => false,
			)
		);

		$settings = array();

		foreach ( $sites as $site ) {

			$current_blog_details = get_blog_details( $site->blog_id );

			$settings[ "province_{$site->blog_id}" ] = array(
				'title'  => $current_blog_details->blogname,
				'fields' => array(
					array(
						'id'    => "enable_cron_{$site->blog_id}",
						'title' => __( 'Enable Cron', 'epra-location-import-tool' ),
						'type'  => 'checkbox',
					),
					array(
						'id'      => "cron_time_{$site->blog_id}",
						'title'   => __( 'Cron Time', 'epra-location-import-tool' ),
						'desc'    => __( 'Choose the time (in GMT) the daily cron will run.', 'epra-location-import-tool' ),
						'type'    => 'dropdown',
						'items'   => $this->get_hours_range(
							array(
								'format' => 'H:i',
							)
						),
						'default' => '04:00',
						'blank'   => true,
					),
					array(
						'id'    => "origin_table_{$site->blog_id}",
						'title' => __( 'Origin Table', 'epra-location-import-tool' ),
						'desc'  => __( 'Choose the table the locations will be loaded from.', 'epra-location-import-tool' ),
						'type'  => 'dropdown',
						'items' => $this->get_location_tables(),
						'blank' => true,
					),
					array(
						'id'      => "export_origin_{$site->blog_id}",
						'title'   => __( 'Export Origin Table', 'epra-location-import-tool' ),
						'desc'    => __( 'Export origin table as UTF-8 CSV.', 'epra-location-import-tool' ),
						'href'    => add_query_arg(
							array(
								'action' => 'export_origin',
								'blogid' => $site->blog_id,
							),
							admin_url( 'admin-ajax.php' )
						),
						'type'    => 'button',
						'classes' => array( 'button-secondary' ),
						'atts'    => array(
							'data-blogid' => $site->blog_id,
						),
						'save'    => false,
					),
					array(
						'id'      => "export_wpsl_{$site->blog_id}",
						'title'   => __( 'Export WPSL Locations', 'epra-location-import-tool' ),
						'desc'    => __( 'Export wpsl_stores as UTF-8 CSV.', 'epra-location-import-tool' ),
						'href'    => add_query_arg(
							array(
								'action' => 'export_wpsl',
								'blogid' => $site->blog_id,
							),
							admin_url( 'admin-ajax.php' )
						),
						'type'    => 'button',
						'classes' => array( 'button-secondary' ),
						'atts'    => array(
							'data-blogid' => $site->blog_id,
						),
						'save'    => false,
					),
					array(
						'id'      => "compare_{$site->blog_id}",
						'title'   => __( 'Run Compare', 'epra-location-import-tool' ),
						'desc'    => __( 'Run the compare function without deleting existing ones.', 'epra-location-import-tool' ),
						'href'    => '#',
						'type'    => 'button',
						'classes' => array( 'button-secondary' ),
						'atts'    => array(
							'data-blogid' => $site->blog_id,
						),
						'save'    => false,
					),
					array(
						'id'      => "refresh_{$site->blog_id}",
						'title'   => __( 'Refresh', 'epra-location-import-tool' ),
						'desc'    => __( 'Delete all existing locations and then import.', 'epra-location-import-tool' ),
						'href'    => '#',
						'type'    => 'button',
						'classes' => array( 'button-secondary' ),
						'atts'    => array(
							'data-blogid' => $site->blog_id,
						),
						'save'    => false,
					),
					array(
						'id'      => "delete_all_{$site->blog_id}",
						'title'   => __( 'Delete All Locations', 'epra-location-import-tool' ),
						'desc'    => __( 'Clear all existing locations.', 'epra-location-import-tool' ),
						'href'    => '#',
						'type'    => 'button',
						'classes' => array( 'delete' ),
						'atts'    => array(
							'data-blogid' => $site->blog_id,
						),
						'save'    => false,
					),
					array(
						'id'            => "allowed_urls_{$site->blog_id}",
						'title'         => __( 'Allowed URLs', 'epra-location-import-tool' ),
						'desc'          => __( 'URLs to be allowed in search, all others will be hidden. Write each URL on a new line. Leave blank to allow all URLs.', 'epra-location-import-tool' ),
						'type'          => 'textarea',
						'blank'         => true,
						'register'      => array(
							'sanitize_callback' => array( $this, 'create_array_from_newlines' ),
						),
						'view_callback' => array( $this, 'create_newlines_from_array' ),
					),
				),
			);
		}

		$options_panel = new Settings_Framework( $menu, $settings );

	}

	/**
	 * Menu item will allow us to load the page to display the location log list table.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @return void
	 */
	public function add_log_submenu() {

		add_submenu_page(
			'epra_location_import_tool',
			__( 'EPRA Location Import Tool Log', 'epra-location-import-tool' ),
			__( 'Log', 'epra-location-import-tool' ),
			'manage_network',
			'epra_location_import_tool_log',
			array( $this, 'log_table_page' )
		);

	}

	/**
	 * Display the list table page.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @return void
	 */
	public function log_table_page() {

		$list = new Logger_List();
		$list->prepare_items();
		$page = ( ! empty( $_REQUEST['page'] ) ) ? sanitize_text_field( wp_unslash( $_REQUEST['page'] ) ) : '';
		?>
			<div class="wrap">
				<h1 class="wp-heading-inline"><?php esc_html_e( 'EPRA Location Import Tool Log', 'epra-location-import-tool' ); ?></h1>
				<form id="location-log-list-form" method="get">
					<input type="hidden" name="page" value="<?php echo esc_attr( $page ); ?>" />
					<?php $list->search_box( __( 'Find', 'epra-location-import-tool' ), 'log-find' ); ?>
					<?php $list->display(); ?>
				</form>
			</div>
		<?php

	}

	/**
	 * Add ability to choose the number of logs per page.
	 *
	 * @since 1.0.0
	 *
	 * @internal Only to be used by screens hook.
	 *
	 * @return void
	 */
	public function add_log_view_options() {

		$option = 'per_page';
		$args   = array(
			'label'   => __( 'Number of logs', 'epra-location-import-tool' ),
			'default' => 10,
			'option'  => 'logs_per_page',
		);
		add_screen_option( $option, $args );

	}

	/**
	 * Display the list table page.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @param mixed $keep   Whether to save or skip saving the screen option value. Default false.
	 * @param mixed $option The option name.
	 * @param int   $value  The number of rows to use.
	 * @return int
	 */
	public function set_log_view_options( $keep, $option, $value ) {
		return $value;
	}

	/**
	 * Get array of hours for time dropdown.
	 *
	 * @since 1.0.0
	 *
	 * @param array $args {
	 *     Optional. An array of arguments.
	 *
	 *     @type int    start  Default 0       The start time, in seconds.
	 *     @type int    end    Default 86400   The end time, in seconds.
	 *     @type int    step   Default 3600    The jump between items, in seconds.
	 *     @type string format Default 'g:i a' The time format displayed in the list.
	 * }
	 * @return array An array of times.
	 */
	public function get_hours_range( array $args = array() ) {

		$defaults = array(
			'start'  => 0,
			'end'    => 86400,
			'step'   => 3600,
			'format' => 'g:i a',
		);

		$args = wp_parse_args( $args, $defaults );

		$times = array();
		foreach ( range( $args['start'], $args['end'], $args['step'] ) as $timestamp ) {
			$hour_mins = gmdate( 'H:i', $timestamp );
			if ( ! empty( $args['format'] ) ) {
				$times[ $hour_mins ] = gmdate( $args['format'], $timestamp );
			} else {
				$times[ $hour_mins ] = $hour_mins;
			}
		}
		return $times;

	}

	/**
	 * Get list of location tables.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @return array An array of the tables found.
	 */
	private function get_location_tables() {

		global $wpdb;

		$output = array();

		$tables = $wpdb->get_results(
			$wpdb->prepare(
				'SHOW TABLES LIKE %s',
				/**
				 * Filters the name of the origin location tables to be searched for.
				 *
				 * @since 1.0.4
				 */
				apply_filters( 'epra_location_import_tool_location_table_query', '%_store_locations' )
			),
			'ARRAY_N'
		);

		foreach ( $tables as $table ) {
			$output[ $table[0] ] = $table[0];
		}

		return $output;

	}

	/**
	 * Create array from strin gusing new lines as delimiter.
	 *
	 * @since 1.0.0
	 *
	 * @param string $input String to be turned into array.
	 * @return array
	 */
	public function create_array_from_newlines( string $input ) {
		return ( ! empty( $input ) && is_string( $input ) ) ? explode( "\n", str_replace( "\r", '', $input ) ) : $input;
	}

	/**
	 * Break up array into string adding a new line between each element.
	 *
	 * @since 1.0.0
	 *
	 * @param array $input Array to be converted to string.
	 * @return string
	 */
	public function create_newlines_from_array( $input ) {
		return ( ! empty( $input ) && is_array( $input ) ) ? implode( "\n", $input ) : $input;
	}

	/**
	 * Display the list table page.
	 *
	 * @since 1.0.0
	 *
	 * @internal Only to be used for enabling/disabling site's location cron.
	 *
	 * @param array $options The plugin's options returned from the database.
	 * @return void
	 */
	public function create_compare_cron_jobs( $options ) {

		// Filter the cron plugin options and return an array of cron enable toggle option.
		$cron_enable_options = array_filter(
			$options,
			function ( $value, $key ) {
				if ( strpos( $key, 'enable_cron_' ) === 0 ) {
					return true;
				}
			},
			ARRAY_FILTER_USE_BOTH
		);

		/*
		* Loop through each option to update the cron job accordingly
		*/
		foreach ( $cron_enable_options as $key => $value ) {
			$site_id = str_replace( 'enable_cron_', '', $key );

			/* Switch to each site and update the cron setting */
			switch_to_blog( $site_id );

			$has_epra_location_cron = wp_get_schedule( 'epra_location_import_hook' );

			// Check if the enable cron toggle value is changed.
			if ( '1' !== $value && false !== $has_epra_location_cron ) {
				// If enable cron toggle is turn off, turn off cron hook.
				wp_clear_scheduled_hook( 'epra_location_import_hook' );

				$timestamp = wp_next_scheduled( 'epra_location_import_hook' );
				wp_unschedule_event( $timestamp, 'epra_location_import_hook' );

				wp_clear_scheduled_hook( 'epra_location_import_hook' );
			} else {
				// If enable cron toggle is turn on, enable cron hooks.
				wp_clear_scheduled_hook( 'epra_location_import_hook' );

				// Get cron time by site id and the hour number for comparison.
				$cron_time_by_site = "cron_time_$site_id";
				$cron_hour         = $options[ $cron_time_by_site ];

				// Compare the cron time to now.
				// If cron hour is before now, begin cron job hour to next day.
				// If cron hour is after now, begin cron job hour today.
				$start_cron_time = ( date( 'H:i' ) < $cron_hour ) ?
					new \DateTime( "today $cron_hour" ) : new \DateTime( "tomorrow $cron_hour" );
				$cron_time       = date_timestamp_get( $start_cron_time );

				if ( ! wp_next_scheduled( 'epra_location_import_hook' ) ) {
					wp_schedule_event( $cron_time, 'daily', 'epra_location_import_hook' );
				}
			}

			restore_current_blog();
		}

	}

}
