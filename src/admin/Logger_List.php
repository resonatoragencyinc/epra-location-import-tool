<?php
/**
 * Extend the WP_List_Table.
 *
 * @since 1.0.0
 *
 * @package    Epra_Location_Import_Tool
 * @subpackage Epra_Location_Import_Tool/includes
 */

namespace Epra_Location_Import_Tool\admin;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}

use \Epra_Location_Import_Tool\includes\Logger;

/**
 * This class will create a new table class that will extend the WP_List_Table to show Location Logs.
 */
class Logger_List extends \WP_List_Table {

	/**
	 * Initialize the WP_List_Table.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		parent::__construct(
			array(
				'singular' => __( 'Location Tool Log', 'epra-location-import-tool' ),
				'plural'   => __( 'Location Tool Logs', 'epra-location-import-tool' ),
				'ajax'     => false,
			)
		);

	}

	/**
	 * Prepare the items for the table to process.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @return void
	 */
	public function prepare_items() {

		add_thickbox();

		$user_search_key = isset( $_REQUEST['s'] ) ? sanitize_text_field( wp_unslash( $_REQUEST['s'] ) ) : '';

		$columns  = $this->get_columns();
		$hidden   = $this->get_hidden_columns();
		$sortable = $this->get_sortable_columns();

		$per_page     = $this->get_items_per_page( 'logs_per_page', 25 );
		$current_page = $this->get_pagenum();
		$total_items  = Logger::get_log_count();
		$orderby      = ( isset( $_GET['orderby'] ) ) ? sanitize_text_field( wp_unslash( $_GET['orderby'] ) ) : '';
		$order        = ( isset( $_GET['order'] ) ) ? sanitize_text_field( wp_unslash( $_GET['order'] ) ) : '';

		$this->set_pagination_args(
			array(
				'total_items' => $total_items,
				'per_page'    => $per_page,
			)
		);

		$args = array(
			'post_parent'    => null,
			'posts_per_page' => $per_page,
			'paged'          => $current_page,
			'log_type'       => false,
			'orderby'        => $orderby,
			'order'          => $order,
		);

		if ( 'meta_value' === $orderby ) {
			$args['meta_key'] = '_location_tool_log_blog_id';
		}

		$this->_column_headers = array( $columns, $hidden, $sortable );
		$this->items           = Logger::get_connected_logs( $args );

		// filter the data in case of a search.
		if ( $user_search_key ) {
			$this->items = $this->filter_table_data( $this->items, $user_search_key );
		}

	} // prepare_items

	/**
	 * Whether the table has items to display or not.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @return bool True if items are found, false otherwise.
	 */
	public function has_items() {
		return ! empty( $this->items );
	} // has_items

	/**
	 * Message to be echoed when there are no items.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @return void
	 */
	public function no_items() {
		esc_html_e( 'No logs to display.', 'epra-location-import-tool' );
	} // no_items

	/**
	 * Override the parent columns method. Defines the columns to use in the list table.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @return array Associatize array of columns.
	 */
	public function get_columns() {

		$columns = array(
			'cb'          => '<input type="checkbox" />',
			'id'          => __( 'Log ID', 'epra-location-import-tool' ),
			'blog'        => __( 'Blog ID', 'epra-location-import-tool' ),
			'title'       => __( 'Function', 'epra-location-import-tool' ),
			'content'     => __( 'View', 'epra-location-import-tool' ),
			'num_of_each' => __( 'A/U/D/E', 'epra-location-import-tool' ),
			'date'        => __( 'Date', 'epra-location-import-tool' ),
			'author'      => __( 'Run By', 'epra-location-import-tool' ),
		);

		return $columns;

	} // get_columns

	/**
	 * Define which columns are hidden.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @return array Associatize array of hidden columns.
	 */
	public function get_hidden_columns() {
		return array();
	} // get_hidden_columns

	/**
	 * Define the sortable columns.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @return array Associatize array of sortable columns.
	 */
	public function get_sortable_columns() {

		return array(
			'blog'   => array( 'meta_value', true ),
			'title'  => array( 'title', true ),
			'date'   => array( 'date', true ),
			'author' => array( 'author', true ),
		);

	} // get_sortable_columns

	/**
	 * Returns an associative array containing the bulk action.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @return array Associatize array of list table actions.
	 */
	public function get_bulk_actions() {
		/*
		 * on hitting apply in bulk actions the url paramas are set as
		 * ?action=bulk-download&paged=1&action2=-1
		 *
		 * action and action2 are set based on the triggers above and below the table
		 */
		$actions = array(
			'bulk-download' => __( 'Export Selected Logs', 'epra-location-import-tool' ),
		);

		return $actions;

	} // get_bulk_actions

	/**
	 * Define what data to show on each column of the table.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @param object $post        Current row object.
	 * @param string $column_name Current column name.
	 * @return mixed
	 */
	public function column_default( $post, $column_name ) {

		/**
		 * Fires for each custom column in the EPRA Location Import Tool Log list table.
		 *
		 * @since 1.0.0
		 *
		 * @param string $column_name The name of the column to display.
		 * @param int    $post_id     The current post ID.
		 */
		do_action( 'manage_location_tool_log_posts_custom_column', $column_name, $post->ID );

	} // column_default

	/**
	 * Hanldes the checkbox column.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @param WP_Post $post The current WP_Post object.
	 * @return void Text to be placed inside the column <td>.
	 */
	public function column_cb( $post ) {

		?>
		<label class="screen-reader-text" for="cb-select-<?php echo esc_attr( $post->ID ); ?>">
			<?php
			// translators: Select (title).
			printf( esc_html__( 'Select %s', 'epra-location-import-tool' ), esc_html( _draft_or_post_title( $post ) ) );
			?>
		</label>
		<input id="cb-select-<?php echo esc_attr( $post->ID ); ?>" type="checkbox" name="post[]" value="<?php echo esc_attr( $post->ID ); ?>" />
		<?php

	} // column_cb

	/**
	 * Handles the id column.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @param \WP_Post $post The current WP_Post object.
	 * @return string Output of the ID column.
	 */
	public function column_id( \WP_Post $post ) {

		/**
		 * Filters the ID column output.
		 *
		 * @since 1.0.0
		 *
		 * @param WP_Post $post        Post object.
		 * @param string  $column_name The column name.
		 */
		return apply_filters(
			'location_tool_log_column_id',
			$post->ID,
			$post,
			'id'
		);

	} // column_id

	/**
	 * Handles the blog id column.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @param \WP_Post $post The current WP_Post object.
	 * @return string Output of the Blog ID column.
	 */
	public function column_blog( \WP_Post $post ) {

		$blog_details = get_blog_details( $post->_location_tool_log_blog_id );
		return $blog_details->domain . $blog_details->path;

	} // column_blog

	/**
	 * Handles the function title column.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @param \WP_Post $post The current WP_Post object.
	 * @return string Output of the Function column.
	 */
	public function column_title( \WP_Post $post ) {

		/**
		 * Filters the title column output.
		 *
		 * @since 1.0.0
		 *
		 * @param WP_Post $post        Post object.
		 * @param string  $column_name The column name.
		 */
		return apply_filters(
			'location_tool_log_column_title',
			$post->post_title,
			$post,
			'title'
		);

	} // column_title

	/**
	 * Handles the view log column.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @param \WP_Post $post The current WP_Post object.
	 * @return string Output of the log content column.
	 */
	public function column_content( \WP_Post $post ) {

		$url = wp_nonce_url(
			add_query_arg(
				array(
					'width'     => '600',
					'height'    => '550',
					'action'    => 'view_location_tool_log',
					'postid'    => $post->ID,
				),
				admin_url( 'admin-ajax.php' )
			),
			'epra_location_import_tool_log_view'
		);

		/**
		 * Filters the view log link output.
		 *
		 * @since 1.0.0
		 *
		 * @param WP_Post $post        Post object.
		 * @param string  $column_name The column name.
		 */
		return apply_filters(
			'location_tool_log_column_content',
			sprintf(
				'<a id="%s" href="%s" class="view_log" data-postid="%d" data-title="%s">' . __( 'View log', 'epra-location-import-tool' ) . '</a>',
				"view_log-{$post->ID}",
				$url,
				$post->ID,
				$post->post_title
			),
			$post,
			'content'
		);

	} // column_content

	/**
	 * Handles the value for number of each log entry type column.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @param \WP_Post $post The current WP_Post object.
	 * @return string Output of the number of each column.
	 */
	public function column_num_of_each( $post ) {

		$added = $post->_location_tool_log_added;
		$updated = $post->_location_tool_log_updated;
		$deleted = $post->_location_tool_log_deleted;
		$errors = $post->_location_tool_log_error;

		/**
		 * Filters the number of each log entry type column output.
		 *
		 * @since 1.0.0
		 *
		 * @param WP_Post $post        Post object.
		 * @param string  $column_name The column name.
		 */
		return apply_filters(
			'location_tool_log_column_num_of_each',
			sprintf(
				'<span class="added">%d</span>/' .
				'<span class="updated">%d</span>/' .
				'<span class="deleted">%d</span>/' .
				'<span class="errors">%d</span>',
				$added,
				$updated,
				$deleted,
				$errors
			),
			$post,
			'num_of_each'
		);

	} // column_num_of_each

	/**
	 * Handles the post date column output.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @param \WP_Post $post The current WP_Post object.
	 * @return string Output of the date column.
	 */
	public function column_date( \WP_Post $post ) {

		if ( '0000-00-00 00:00:00' === $post->post_date ) {
			$t_time    = __( 'Unpublished', 'epra-location-import-tool' );
			$h_time    = __( 'Unpublished', 'epra-location-import-tool' );
			$time_diff = 0;
		} else {
			$t_time = get_the_time( __( 'Y/m/d g:i:s a', 'epra-location-import-tool' ), $post );
			$m_time = $post->post_date;
			$time   = get_post_time( 'G', true, $post );

			$time_diff = time() - $time;

			if ( $time_diff > 0 && $time_diff < DAY_IN_SECONDS ) {
				// translators: time ago.
				$h_time = sprintf( __( '%s ago', 'epra-location-import-tool' ), human_time_diff( $time ) );
			} else {
				$h_time = mysql2date( __( 'Y/m/d', 'epra-location-import-tool' ), $m_time );
			}
		}

		/**
		 * Filters the published time of the post column output.
		 *
		 * @since 1.0.0
		 *
		 * @param string  $h_time      The published time.
		 * @param WP_Post $post        Post object.
		 * @param string  $column_name The column name.
		 */
		return '<abbr title="' . esc_attr( $t_time ) . '">' . esc_html( apply_filters( 'location_tool_log_column_date', $h_time, $post, 'date' ) ) . '</abbr>';

	} // column_date

	/**
	 * Handles the post author column output.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @param \WP_Post $post The current WP_Post object.
	 * @return string Output of the Run By column.
	 */
	public function column_author( \WP_Post $post ) {

		if ( ! empty( $post->_location_tool_log_run_by_cron ) ) {
			return 'WP Cron';
		}

		$url = get_edit_user_link( $post->post_author );
		$label = get_userdata( $post->post_author )->display_name;

		/**
		 * Filters the author column output.
		 *
		 * @since 1.0.0
		 *
		 * @param WP_Post $post        Post object.
		 * @param string  $column_name The column name.
		 */
		return apply_filters(
			'location_tool_log_column_author',
			sprintf(
				'<a href="%s">%s</a>',
				esc_url( $url ),
				esc_html( $label )
			),
			$post,
			'author'
		);

	} // column_author

	/**
	 * Filter the table data based on the search key.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @param array  $table_data An array of WP_Post objects.
	 * @param string $search_key The search query.
	 * @return array Array of WP_Post objects, filtered by search key.
	 */
	private function filter_table_data( array $table_data, string $search_key ) {

		$filtered_table_data = array_values(
			array_filter(
				$table_data,
				function( $row ) use ( $search_key ) {
					foreach ( $row as $row_val ) {
						if ( stripos( $row_val, $search_key ) !== false ) {
							return true;
						}
					}
				}
			)
		);

		return $filtered_table_data;

	} // filter_table_data

} // Epra_Location_Import_Tool_Logger_List
