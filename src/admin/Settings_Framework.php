<?php
/**
 * The settings framework.
 *
 * @since 1.0.0
 *
 * @package    Epra_Location_Import_Tool
 * @subpackage Epra_Location_Import_Tool/admin
 */

namespace Epra_Location_Import_Tool\admin;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Defines the settings framework to create menu pages and register settings.
 */
class Settings_Framework {

	/**
	 * The ID of this plugin.
	 *
	 * @since 1.0.0
	 *
	 * @var array $menu The ID of this plugin.
	 */
	private $menu;

	/**
	 * The settings of this plugin.
	 *
	 * @since 1.0.0
	 *
	 * @var array $settings The current settings of this plugin.
	 */
	private $settings;

	/**
	 * The options saved to the db.
	 *
	 * @since 1.0.0
	 *
	 * @var array $options The current options save to the db.
	 */
	public $options;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since 1.0.0
	 *
	 * @param string $menu     The menu to be created.
	 * @param string $settings The settings to be displayed.
	 */
	public function __construct( $menu, $settings ) {

		$this->menu     = $menu;
		$this->settings = $settings;
		$this->options  = get_site_option( $this->menu['slug'] );

		add_action( 'network_admin_menu', array( $this, 'register_menu' ) );
		add_action( "network_admin_edit_{$this->menu['slug']}_update_network_options", array( $this, 'update_network_options' ) );

	}

	/**
	 * Undocumented function
	 *
	 * @since 1.0.0
	 *
	 * @internal Only to be used by network admin menu hook.
	 *
	 * @return void
	 */
	public function register_menu() {

		add_menu_page(
			$this->menu['page_title'],
			$this->menu['menu_title'],
			$this->menu['capability'],
			$this->menu['slug'],
			array( $this, 'page_callback' ),
			$this->menu['icon']
		);

		$this->register_settings();

	} // register_menu

	/**
	 * Undocumented function
	 *
	 * @since 1.0.0
	 *
	 * @internal Only to be used by self::register_menu().
	 *
	 * @return void
	 */
	private function register_settings() {

		foreach ( $this->settings as $id => $values ) {

			add_settings_section(
				$id,
				$values['title'] ?? '',
				$values['desc'] ?? null,
				$this->menu['slug']
			);

			// Loop through the fields to add different fields.
			foreach ( $values['fields'] as $field ) {

				switch ( $field['type'] ) {
					case 'dropdown':
						add_settings_field(
							$field['id'],
							$field['title'] ?? '',
							array( $this, $field['type'] ),
							$this->menu['slug'],
							$id,
							array(
								'name'      => $field['title'] ?? '',
								'id'        => $field['id'],
								'desc'      => $field['desc'] ?? '',
								'items'     => $field['items'] ?? array(),
								'blank'     => $field['blank'] ?? false,
								'default'   => $field['default'] ?? '',
								'label_for' => $field['id'],
							)
						);
						break;

					case 'button':
						add_settings_field(
							$field['id'],
							$field['title'] ?? '',
							array( $this, $field['type'] ),
							$this->menu['slug'],
							$id,
							array(
								'title'   => $field['title'] ?? '',
								'id'      => $field['id'],
								'desc'    => $field['desc'] ?? '',
								'classes' => $field['classes'] ?? '',
								'href'    => $field['href'] ?? '',
								'target'  => $field['target'] ?? '_self',
								'atts'    => $field['atts'] ?? array(),
							)
						);
						break;

					case 'textarea':
					case 'textbox':
					case 'password':
						add_settings_field(
							$field['id'],
							$field['title'] ?? '',
							array( $this, $field['type'] ),
							$this->menu['slug'],
							$id,
							array(
								'name'          => $field['title'] ?? '',
								'id'            => $field['id'],
								'desc'          => $field['desc'] ?? '',
								'default'       => $field['default'] ?? '',
								'label_for'     => $field['id'],
								'view_callback' => $field['view_callback'] ?? null,
							)
						);
						break;

					default:
						add_settings_field(
							$field['id'],
							$field['title'] ?? '',
							array( $this, $field['type'] ),
							$this->menu['slug'],
							$id,
							array(
								'name'      => $field['title'] ?? '',
								'id'        => $field['id'],
								'desc'      => $field['desc'] ?? '',
								'default'   => $field['default'] ?? '',
								'label_for' => $field['id'],
							)
						);
						break;

				}

				register_setting( $this->menu['slug'], $field['id'], $field['register'] ?? array() );
			}
		}

	} // register_settings

	/**
	 * Displays a checkbox.
	 *
	 * @since 1.0.0
	 *
	 * @param array $args {
	 *     An array of arguments.
	 *
	 *     @type string id   Element ID.
	 *     @type string desc Description displayed underneath the checkbox.
	 * }
	 *
	 * @return void
	 */
	public function checkbox( array $args ) {

		$defaults = array(
			'id'   => '',
			'desc' => '',
		);

		$args = wp_parse_args( $args, $defaults );

		$value = $this->options[ $args['id'] ] ?? false;
		?>
		<label>
			<input type="checkbox" id="<?php echo esc_attr( $args['id'] ); ?>"
				name="<?php echo esc_attr( "{$this->menu['slug']}[{$args['id']}]" ); ?>"
				value="1" <?php checked( $value ); ?> />
				<?php if ( $args['desc'] ) : ?>
					<p id="<?php echo esc_attr( $args['id'] ); ?>-desc" class="description"><?php echo esc_html( $args['desc'] ); ?></p>
				<?php endif; ?>
		</label>
		<?php

	} // checkbox

	/**
	 * Displays a select dropdown.
	 *
	 * @since 1.0.0
	 *
	 * @param array $args {
	 *     An array of arguments.
	 *
	 *     @type string id      Element ID.
	 *     @type string desc    Description displayed underneath the dropdown.
	 *     @type array  items   Array of items for the dropdown. $key is the option's value, $value is the displayed text.
	 *     @type bool   blank   If true will set the first option as blank.
	 *     @type string default Default value to be selected, if blank.
	 * }
	 *
	 * @return void
	 */
	public function dropdown( array $args ) {

		$defaults = array(
			'id'      => '',
			'desc'    => '',
			'items'   => array(),
			'blank'   => false,
			'default' => '',
		);

		$args = wp_parse_args( $args, $defaults );

		$value = $this->options[ $args['id'] ] ?? $args['default'];
		?>
		<select id="<?php echo esc_attr( $args['id'] ); ?>" name="<?php echo esc_attr( "{$this->menu['slug']}[{$args['id']}]" ); ?>">
		<?php
		if ( $args['blank'] ) :
			?>
			<option value=''></option>
			<?php
		endif;

		foreach ( $args['items'] as $item ) :
			?>
			<option value="<?php echo esc_attr( $item ); ?>" <?php selected( $value, $item ); ?>><?php echo esc_attr( $item ); ?></option>
			<?php
		endforeach;
		?>
		</select>
		<?php if ( $args['desc'] ) : ?>
			<p id="<?php echo esc_attr( $args['id'] ); ?>-desc" class="description"><?php echo esc_html( $args['desc'] ); ?></p>
		<?php endif; ?>
		<?php

	} // dropdown

	/**
	 * Displays a textarea.
	 *
	 * @since 1.0.0
	 *
	 * @param array $args {
	 *     An array of arguments.
	 *
	 *     @type string id      Element ID.
	 *     @type string desc    Description displayed underneath the input.
	 *     @type string default Default text to be in the field, if blank.
	 * }
	 *
	 * @return void
	 */
	public function textarea( array $args ) {

		$defaults = array(
			'id'            => '',
			'desc'          => '',
			'default'       => '',
			'view_callback' => null,
		);

		$args = wp_parse_args( $args, $defaults );

		$text = $this->options[ $args['id'] ] ?? $args['default'];
		$text = ( $args['view_callback'] ) ? call_user_func( $args['view_callback'], $text ) : $text;

		?>
		<textarea id="<?php echo esc_attr( $args['id'] ); ?>" name="<?php echo esc_attr( "{$this->menu['slug']}[{$args['id']}]" ); ?>"
			rows="7" cols="50" type="textarea"><?php echo esc_textarea( $text ); ?></textarea>
		<?php if ( $args['desc'] ) : ?>
			<p id="<?php echo esc_attr( $args['id'] ); ?>-desc" class="description"><?php echo esc_html( $args['desc'] ); ?></p>
		<?php endif; ?>
		<?php

	} // textarea

	/**
	 * Displays a text field.
	 *
	 * @since 1.0.0
	 *
	 * @param array $args {
	 *     An array of arguments.
	 *
	 *     @type string id      Element ID.
	 *     @type string desc    Description displayed underneath the input.
	 *     @type string default Default text to be in the field, if blank.
	 * }
	 *
	 * @return void
	 */
	public function textbox( array $args ) {

		$defaults = array(
			'id'            => '',
			'desc'          => '',
			'default'       => '',
			'view_callback' => null,
		);

		$args = wp_parse_args( $args, $defaults );

		$text = $this->options[ $args['id'] ] ?? $args['default'];

		if ( $args['view_callback'] ) {
			$text = call_user_func( $args['view_callback'], $text );
		}
		?>
			<input id="<?php echo esc_attr( $args['id'] ); ?>" name="<?php echo esc_attr( "{$this->menu['slug']}[{$args['id']}]" ); ?>"
				size="40" type="text" value="<?php echo esc_textarea( $text ); ?>" />
			<?php if ( $args['desc'] ) : ?>
				<p id="<?php echo esc_attr( $args['id'] ); ?>-desc" class="description"><?php echo esc_html( $args['desc'] ); ?></p>
			<?php endif; ?>
		<?php

	} // textbox

	/**
	 * Displays a password field.
	 *
	 * @since 1.0.0
	 *
	 * @param array $args {
	 *     An array of arguments.
	 *
	 *     @type string id   Element ID.
	 *     @type string desc Description displayed underneath the input.
	 *     @type string default Default text to be in the field, if blank.
	 * }
	 *
	 * @return void
	 */
	public function password( array $args ) {

		$defaults = array(
			'id'            => '',
			'desc'          => '',
			'default'       => '',
			'view_callback' => null,
		);

		$args = wp_parse_args( $args, $defaults );

		$text = $this->options[ $args['id'] ] ?? $args['default'];

		if ( $args['view_callback'] ) {
			$text = call_user_func( $args['view_callback'], $text );
		}
		?>
		<textarea id="<?php echo esc_attr( $args['id'] ); ?>" name="<?php echo esc_attr( "{$this->menu['slug']}[{$args['id']}]" ); ?>"
			size="40" type="password"><?php echo esc_textarea( $text ); ?></textarea>
		<?php if ( $args['desc'] ) : ?>
			<p id="<?php echo esc_attr( $args['id'] ); ?>-desc" class="description"><?php echo esc_html( $args['desc'] ); ?></p>
		<?php endif; ?>
		<?php

	} // password

	/**
	 * Displays radio button(s).
	 *
	 * @since 1.0.0
	 *
	 * @param array $args {
	 *     An array of arguments.
	 *
	 *     @type string id      Element ID.
	 *     @type string desc    Description displayed underneath the input.
	 *     @type array  items   Array of items for the dropdown. $key is the option's value, $value is the displayed text.
	 *     @type string default Default value to be selecteded, if blank.
	 * }
	 *
	 * @return void
	 */
	public function radio( array $args ) {

		$defaults = array(
			'id'      => '',
			'desc'    => '',
			'items'   => array(),
			'default' => '',
		);

		$args = wp_parse_args( $args, $defaults );

		$value = $this->options[ $args['id'] ] ?? $args['default'];

		foreach ( $args['items'] as $item ) :
			?>
				<label>
					<input type="radio" <?php checked( $value, $item ); ?> value="<?php echo esc_attr( $item ); ?>"
						name="<?php echo esc_attr( "{$this->menu['slug']}[{$args['id']}]" ); ?>" />
					<?php echo esc_attr( $item ); ?>
				</label><br />
				<?php if ( $args['desc'] ) : ?>
					<p id="<?php echo esc_attr( $args['id'] ); ?>-desc" class="description"><?php echo esc_html( $args['desc'] ); ?></p>
				<?php endif; ?>
			<?php
		endforeach;

	} // radio

	/**
	 * Displays a button.
	 *
	 * @since 1.0.0
	 *
	 * @param array $args {
	 *     An array of arguments.
	 *
	 *     @type string array|string classes An array or string of classes.
	 *     @type string array|string atts    An array or string of attributes.
	 *     @type string              href    The URL.
	 *     @type string              target  Target attribute.
	 *     @type string              id      Element ID.
	 *     @type string              title   Button text.
	 *     @type string              desc    Description displayed underneath the checkbox.
	 * }
	 *
	 * @return void
	 */
	public function button( array $args ) {

		$defaults = array(
			'classes' => 'button-secondary',
			'atts'    => '',
			'href'    => '#',
			'target'  => '_self',
			'id'      => '',
			'title'   => 'Button Text',
			'desc'    => '',
		);

		$args = wp_parse_args( $args, $defaults );

		$classes = '';
		if ( is_array( $args['classes'] ) ) {
			$classes = implode( ' ', $args['classes'] );
		} elseif ( ! empty( $args['classes'] ) ) {
			$classes = $args['classes'];
		} else {
			$classes = '';
		}

		$attributes = '';
		if ( is_array( $args['atts'] ) ) {
			foreach ( $args['atts'] as $attribute => $value ) {
				$attributes .= $attribute . '=' . esc_attr( $value ) . ' '; // Trailing space is important.
			}
		} elseif ( ! empty( $args['atts'] ) ) { // Attributes provided as a string.
			$attributes = $args['atts'];
		}

		?>
		<a href="<?php echo esc_url( "{$args['href']}" ); ?>" target="<?php echo esc_attr( "{$args['target']}" ); ?>"
			class="button <?php echo esc_attr( "{$classes}" ); ?>" <?php echo esc_html( $attributes ); ?>
			id="<?php echo esc_attr( $args['id'] ); ?>"/>
				<?php echo esc_html( $args['title'] ); ?>
		</a>

		<?php if ( $args['desc'] ) : ?>
			<p id="<?php echo esc_attr( $args['id'] ); ?>-desc" class="description"><?php echo esc_html( $args['desc'] ); ?></p>
			<?php
		endif;

	} // button

	/**
	 * Displays the options page.
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function page_callback() {

		if ( isset( $_GET['updated'] ) ) :
			?>
				<div id="message" class="updated notice is-dismissible">
					<p><?php esc_html_e( 'Options saved.', 'epra-location-import-tool' ); ?></p>
				</div>
			<?php
		endif;

		if ( ! empty( $this->menu['template'] ) ) {
			include $this->menu['template'];
		} else {
			?>
			<h1><?php echo esc_html( $this->menu['page_title'] ); ?></h1>
			<?php do_action( "setting_page_description_{$this->menu['slug']}" ); ?>
			<form method="POST" action="edit.php?action=epra_location_import_tool_update_network_options">
			<?php
				settings_fields( $this->menu['slug'] );

			foreach ( $this->settings as $section => $values ) :
				?>
				<?php
				if ( ! empty( $values['title'] ) ) :
					?>
					<a id="<?php echo esc_attr( $section ); ?>"></a>
					<h2><?php echo esc_html( $values['title'] ); ?></h2>
					<?php
				endif;

				if ( ! empty( $values['callback'] ) ) {
					call_user_func( $values['callback'], $values );
				}
				?>
				<table class="form-table">
					<?php
					do_action( "before_fields_{$this->menu['slug']}_$section" );
					do_settings_fields( $this->menu['slug'], $section );
					do_action( "after_fields_{$this->menu['slug']}_$section" );
					?>
				</table>
				<?php
				submit_button();
			endforeach;

			?>
			</div>
			</form>
			<?php
		}

	} // page_callback

	/**
	 * This function here is hooked up to a special action and necessary to process
	 * the saving of the options. This is the big difference with a normal options
	 * page.
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function update_network_options() {

		check_admin_referer( "{$this->menu['slug']}-options" );

		// This is the list of registered options.
		global $new_whitelist_options, $wp_registered_settings;
		$fields = $new_whitelist_options[ $this->menu['slug'] ];

		// Go through the posted data and save only our options.
		$options = array();

		foreach ( $fields as $field ) {
			if ( isset( $_POST[ $this->menu['slug'] ][ $field ] ) ) {
				$options[ $field ] = sanitize_option( $field, wp_unslash( $_POST[ $this->menu['slug'] ][ $field ] ) );
			}
		}

		do_action( "before_option_update_{$this->menu['slug']}", $options );

		// Save our option with the site's options.
		update_site_option( $this->menu['slug'], $options );

		do_action( "after_option_update_{$this->menu['slug']}", $options );

		// At last we redirect back to our options page.
		wp_redirect(
			add_query_arg(
				array(
					'page'    => $this->menu['slug'],
					'updated' => 'true',
				),
				network_admin_url( 'admin.php' )
			)
		);

		die();

	} // update_network_options

} // Epra_Location_Import_Tool_Settings_Framework
