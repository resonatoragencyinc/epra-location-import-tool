(function( $ ) {
	'use strict';

	var modalTitle = $('#modal-ajax-title');
	var modalContent = $('#modal-ajax-content');

	// Generic function to make an AJAX call
	var fetchData = function(query, dataURL) {
		// Return the $.ajax promise
		return $.ajax({
			data: query,
			dataType: 'json',
			url: dataURL,
			type: 'POST'
		});
	}

	// Epra_Location_Import_Tool_Ajax::delete_locations()
	$('a[id^=delete_all_]').on('click', function(e) {
		e.preventDefault();

		var confirm = window.confirm('This will remove all locations.\nAre you absolutly sure this is what you are trying to do?');
		if (!confirm) {
			return;
		}

		MicroModal.init({
			disableFocus: true
		});
		MicroModal.show('modal-ajax');
		modalContent.html('Working...');

		var button = $(this);
		var orginalText = button.text();

		button.attr('disabled', true);

		modalTitle.html(orginalText);

		var data = {
			'action': 'delete_locations',
			'security': $('#_wpnonce').attr('value'),
			'blogid': button.data('blogid'),
		};

		var response = fetchData(data, ajaxurl);

		response.always(function(){
			button.attr('disabled', false);
		});
		response.done(function(e) {
			if (e.success) {
				modalContent.append(`<span class="deleted">${e.data.count.deleted}</span>/<span class="errors">${e.data.count.error}</span>`);
				modalContent.append(e.data.message);
			} else {
				modalContent.append(`<p class="description error">${e.message}</p>`);
			}
		});
		response.fail(function(jqXHR, textStatus, errorThrown) {
			modalContent.html('<p class="description error">' + textStatus + ': ' + errorThrown + '</p>');
		});
	});

	// Epra_Location_Import_Tool_Ajax::refresh_locations()
	$('a[id^=compare_]').on('click', function(e) {
		e.preventDefault();

		MicroModal.init({
			disableFocus: true
		});
		MicroModal.show('modal-ajax');
		modalContent.html('Working...');

		var button = $(this);
		var orginalText = button.text();

		button.attr('disabled', true);

		modalTitle.html(orginalText);

		var data = {
			'action': 'refresh_locations',
			'security': $('#_wpnonce').attr('value'),
			'blogid': $(this).attr('data-blogid'),
			'delete': false
		};

		var response = fetchData(data, ajaxurl);

		response.always(function(){
			button.attr('disabled', false);
		});
		response.done(function(e) {
			if (e.success) {
				modalContent.append(`<span class="added">${e.data.count.added}</span>/<span class="updated">${e.data.count.updated}</span>/<span class="deleted">${e.data.count.deleted}</span>/<span class="errors">${e.data.count.error}</span>`);
				modalContent.append(e.data.message);
			} else {
				modalContent.append(`<p class="description error">${e.message}</p>`);
			}
		});
		response.fail(function(jqXHR, textStatus, errorThrown) {
			modalContent.html('<p class="description error">' + textStatus + ': ' + errorThrown + '</p>');
		});
	});

	// Epra_Location_Import_Tool_Ajax::refresh_locations()
	$('a[id^=refresh_]').on('click', function(e) {
		e.preventDefault();

		MicroModal.init({
			disableFocus: true
		});
		MicroModal.show('modal-ajax');
		modalContent.html('Working...');

		var button = $(this);
		var orginalText = button.text();

		button.attr('disabled', true);

		modalTitle.html(orginalText);

		var data = {
			'action': 'refresh_locations',
			'security': $('#_wpnonce').attr('value'),
			'blogid': $(this).attr('data-blogid'),
			'delete': true
		};

		var response = fetchData(data, ajaxurl);

		response.always(function(){
			button.attr('disabled', false);
		});
		response.done(function(e) {
			if (e.success) {
				modalContent.append(`<span class="added">${e.data.count.added}</span>/<span class="updated">${e.data.count.updated}</span>/<span class="deleted">${e.data.count.deleted}</span>/<span class="errors">${e.data.count.error}</span>`);
				modalContent.append(e.data.message);
			} else {
				modalContent.append(`<p class="description error">${e.message}</p>`);
			}
		});
		response.fail(function(jqXHR, textStatus, errorThrown) {
			modalContent.html('<p class="description error">' + textStatus + ': ' + errorThrown + '</p>');
		});
	});

	// Epra_Location_Import_Tool_Ajax::export_origin()
	// Adds nonce string to button href.
	$('a[id^=export_origin_]').attr('href', function() {
		return $(this).attr('href') + '&_wpnonce=' + $('#_wpnonce').attr('value');
	});

	// Epra_Location_Import_Tool_Ajax::export_wpsl()
	// Adds nonce string to button href.
	$('a[id^=export_wpsl_]').attr('href', function() {
		return $(this).attr('href') + '&_wpnonce=' + $('#_wpnonce').attr('value');
	});

	// Epra_Location_Import_Tool_Ajax::view_location_tool_log()
	// Opens the log in a thickbox.
	$('.view_log').on('click', function(e) {
		e.preventDefault();

		tb_show($(this).data('title'), $(this).attr('href'));
		return false;
	});

})( jQuery );

