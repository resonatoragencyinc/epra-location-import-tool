<?php
/**
 * Provide a admin area view for the plugin.
 *
 * @since 1.0.0
 *
 * @package    Epra_Location_Import_Tool
 * @subpackage Epra_Location_Import_Tool/admin/partials
 */

namespace Epra_Location_Import_Tool\admin\partials;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * This file is used to markup the admin-facing aspects of the plugin.
 */
?>

<div class="wrap">
	<h1><?php echo esc_html( $this->menu['page_title'] ); ?></h1>
	<?php do_action( "setting_page_description_{$this->menu['slug']}" ); ?>
	<form method="POST" action="edit.php?action=epra_location_import_tool_update_network_options">
	<?php
		settings_fields( $this->menu['slug'] );
	?>
	<div class="<?php echo esc_attr( $this->menu['slug'] ); ?>-grid">
	<ul data-tabs>
	<?php
	foreach ( $this->settings as $section => $values ) :
		$open = ( array_key_first( $this->settings ) === $section ) ? 'data-tabby-default' : '';
		?>
		<li>
			<a href="#<?php echo esc_attr( $section ); ?>" <?php echo esc_attr( $open ); ?>>
				<?php echo esc_html( $values['title'] ); ?>
			</a>
		</li>
		<?php
	endforeach;
	?>
	</ul>
	<?php

	foreach ( $this->settings as $section => $values ) :
		?>
		<div id="<?php echo esc_attr( $section ); ?>" class="group">
			<?php
			if ( ! empty( $values['title'] ) ) :
				?>
				<h2><?php echo esc_html( $values['title'] ); ?></h2>
				<?php
			endif;

			if ( ! empty( $values['callback'] ) ) {
				call_user_func( $values['callback'], $values );
			}
			?>
			<table class="form-table">
				<?php do_settings_fields( $this->menu['slug'], $section ); ?>
			</table>
			<?php submit_button(); ?>
		</div>
		<?php
	endforeach;
	?>
	</div>
	</form>
	<div class="modal micromodal-slide" id="modal-ajax" aria-hidden="true">
		<div class="modal__overlay" tabindex="-1">
			<div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-ajax-title">
				<header class="modal__header">
					<h2 class="modal__title" id="modal-ajax-title"></h2>
				</header>
				<main class="modal__content" id="modal-ajax-content">
					<p><?php __( 'Working...', 'epra-location-import-tool' ); ?></p>
				</main>
				<footer class="modal__footer">
					<button class="modal__btn" data-micromodal-close aria-label="Close this dialog window">Close</button>
				</footer>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	// initilize tabs
	var tabs = new Tabby('[data-tabs]');
</script>
