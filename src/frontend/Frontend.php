<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @since 1.0.0
 *
 * @package    Epra_Location_Import_Tool
 * @subpackage Epra_Location_Import_Tool/frontend
 */

namespace Epra_Location_Import_Tool\frontend;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * This class defines the public-facing functionality of the plugin.
 */
class Frontend {

	/**
	 * The ID of this plugin.
	 *
	 * @since 1.0.0
	 *
	 * @var string $epra_location_import_tool The ID of this plugin.
	 */
	private $epra_location_import_tool;

	/**
	 * The version of this plugin.
	 *
	 * @since 1.0.0
	 *
	 * @var string $version The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since 1.0.0
	 *
	 * @param string $epra_location_import_tool The name of the plugin.
	 * @param string $version                   The version of this plugin.
	 */
	public function __construct( $epra_location_import_tool, $version ) {

		$this->epra_location_import_tool = $epra_location_import_tool;
		$this->version                   = $version;

	}

	/**
	 * Remove URLs not specified in the site's options from the returned
	 * post's meta before it is shown on in the public locator tool.
	 *
	 * @since 1.0.1
	 *
	 * @internal Only to be used by WPSL post meta hook.
	 *
	 * @param array $store_meta Associative array of post's meta fields.
	 * @param int   $store_id The ID of the post.
	 * @return array Associative array of post's meta fields.
	 */
	public function strip_restricted_urls_from_meta( $store_meta, $store_id ) {

		if ( ! empty( $store_meta['url'] ) ) {
			$allowed = false;

			if ( empty ( \Epra_Location_Import_Tool\Base::get_options()[ 'allowed_urls_' . get_current_blog_id() ] ) ) {
				return $store_meta;
			}
			
			foreach ( \Epra_Location_Import_Tool\Base::get_options()[ 'allowed_urls_' . get_current_blog_id() ] as $url ) {
				if ( strpos( $store_meta['url'], $url ) ) {
					$allowed = true;
					continue;
				}
			}

			if ( ! $allowed ) {
				$store_meta['url'] = '';
			}
		}

		return $store_meta;

	}

}
