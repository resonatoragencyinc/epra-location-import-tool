<?php
/**
 * Fired during plugin activation.
 *
 * @since 1.0.0
 *
 * @package    Epra_Location_Import_Tool
 * @subpackage Epra_Location_Import_Tool/includes
 */

namespace Epra_Location_Import_Tool\includes;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * This class defines all code necessary to run during the plugin's activation.
 */
class Activator {

	/**
	 * Run on activation.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @return void
	 */
	public static function activate() {
		// do nothing.
	}

}
