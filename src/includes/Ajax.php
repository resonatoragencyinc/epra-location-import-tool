<?php
/**
 * Defines the AJAX functions.
 *
 * @since 1.0.0
 *
 * @package    Epra_Location_Import_Tool
 * @subpackage Epra_Location_Import_Tool/includes
 */

namespace Epra_Location_Import_Tool\includes;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

use \Epra_Location_Import_Tool\includes\Importer;
use \League\Csv\Writer;
use \ForceUTF8\Encoding;

/**
 * This class defines all the function to be run by AJAX calls.
 */
class Ajax {

	/**
	 * Instance of this class.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	protected static $instance = null;

	/**
	 * Loader class to allow public access.
	 *
	 * @since 1.0.0
	 *
	 * @var Importer $importer The importer class.
	 */
	private $importer;

	/**
	 * Initialize the singleton class
	 *
	 * @since 1.0.0
	 */
	private function __construct() {

		$this->importer = new Importer();

	}

	/**
	 * Return an instance of this class.
	 *
	 * @since 1.0.0
	 *
	 * @return object A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Handle AJAX: Delete all locations from selected blog.
	 *
	 * @since 1.0.0
	 *
	 * @internal Only to be used by AJAX.
	 *
	 * @return void
	 */
	public function delete_locations() {

		if ( ! current_user_can( 'manage_network' ) ) {
			wp_send_json_error( self::error_message( 'permission' ) );
		}

		// Security check.
		check_ajax_referer( 'epra_location_import_tool-options', 'security' );

		$blogid = ( isset( $_POST['blogid'] ) ) ? (int) $_POST['blogid'] : wp_send_json_error( self::error_message( 'no_blogid' ) );

		$args = array(
			'blog_id'   => $blogid,
			'delete'    => true,
			'log_title' => 'Delete All Locations',
		);

		try {
			$response = $this->importer->epra_location_import( $args );

		} catch ( Exception $e ) {
			wp_send_json_error( $e->getMessage() );
		}

		// Send response in JSON format..
		wp_send_json_success( $response );

	} // delete_locations

	/**
	 * Handle AJAX: Delete and then add all locations back into WordPress.
	 *
	 * @since 1.0.0
	 *
	 * @internal Only to be used by AJAX.
	 *
	 * @return void
	 */
	public function refresh_locations() {

		if ( ! current_user_can( 'manage_network' ) ) {
			wp_send_json_error( self::error_message( 'permission' ) );
		}

		// Security check.
		check_ajax_referer( 'epra_location_import_tool-options', 'security' );

		$blogid = ( isset( $_POST['blogid'] ) ) ? (int) $_POST['blogid'] : wp_send_json_error( self::error_message( 'no_blogid' ) );

		$args = array(
			'blog_id'   => $blogid,
			'table'     => ( get_site_option( 'epra_location_import_tool' )[ "origin_table_$blogid" ] ) ?? wp_send_json_error( self::error_message( 'no_table' ) ),
			'log_title' => 'Refresh Locations',
		);

		$delete = ( isset( $_POST['delete'] ) ) ? sanitize_text_field( wp_unslash( $_POST['delete'] ) ) : '';

		if ( 'true' === $delete ) {
			$args['delete']     = true;
			$args['log_title'] .= ' + Delete All';
		}

		try {
			$response = $this->importer->epra_location_import( $args );
		} catch ( Exception $e ) {
			wp_send_json_error( $e->getMessage() );
		}

		// Send response in JSON format.
		wp_send_json_success( $response );

	} // refresh_locations

	/**
	 * Handle AJAX: Export the locations from the origin table as CSV
	 *
	 * @since 1.0.0
	 * @global object wpdb
	 *
	 * @internal Only to be used by AJAX.
	 *
	 * @todo Get check_ajax_referer to work.
	 *
	 * @return void
	 */
	public function export_origin() {

		if ( ! current_user_can( 'manage_network' ) ) {
			wp_send_json_error( self::error_message( 'permission' ) );
		}

		// Security check.
		// check_ajax_referer( 'epra_location_import_tool_export_origin', 'security' );

		global $wpdb;

		$blogid = ( isset( $_GET['blogid'] ) ) ? (int) $_GET['blogid'] : wp_send_json_error( self::error_message( 'no_blogid' ) );
		$table  = ( get_site_option( 'epra_location_import_tool' )[ "origin_table_$blogid" ] ) ?? wp_send_json_error( self::error_message( 'no_table' ) );

		if ( $table !== $wpdb->get_var( $wpdb->prepare( 'SHOW TABLES LIKE %s', $table ) ) ) {
			wp_send_json_error( self::error_message( 'table_null' ) );
		}

		// phpcs:disable
		$existing_columns = $wpdb->get_col( "DESC $table", 0 );
		$data             = $wpdb->get_results( "SELECT * FROM $table", ARRAY_N );
		// phpcs:enable

		// load the CSV document from a string.
		$csv = Writer::createFromString( '' );

		// insert the header.
		$csv->insertOne( $existing_columns );

		// insert all the records.
		$csv->insertAll( $data );

		header( 'Content-Type: text/csv; charset=UTF-8' );
		header( 'Content-Description: File Transfer' );
		header( 'Content-Disposition: attachment; filename="' . $table . date( '-d-m-y' ) . '.csv"' );

		Encoding::toUTF8( Encoding::fixUTF8( $csv->output() ) );

		wp_die();

	} // export_origin

	/**
	 * Handle AJAX: Export the post from post_type wpsl_stores
	 *
	 * @since 1.0.0
	 * @global object wpdb
	 *
	 * @internal Only to be used by AJAX.
	 *
	 * @todo Get check_ajax_referer to work.
	 *
	 * @return void
	 */
	public function export_wpsl() {

		if ( ! current_user_can( 'manage_network' ) ) {
			wp_send_json_error( self::error_message( 'permission' ) );
		}

		// Security check.
		// check_ajax_referer( 'epra_location_import_tool-options', 'security' );

		switch_to_blog( ( isset( $_GET['blogid'] ) ) ? (int) $_GET['blogid'] : wp_send_json_error( self::error_message( 'no_blogid' ) ) );

		global $wpdb;

		$headers = array(
			'sl_id',
			'sl_store',
			'sl_address',
			'sl_address2',
			'sl_city',
			'sl_state',
			'sl_zip',
			'sl_country',
			'sl_latitude',
			'sl_longitude',
			'sl_description',
			'sl_tags',
			'sl_url',
			'sl_phone',
		);

		$data = $wpdb->get_results(
			"SELECT
				CONVERT(MAX( IF(m.meta_key = 'db_id', m.meta_value, '')), UNSIGNED INTEGER) AS sl_id,
				REPLACE(p.post_title, '&amp;', '&') AS sl_store,
				MAX( IF(m.meta_key = 'wpsl_address', m.meta_value, '')) AS sl_address,
				MAX( IF(m.meta_key = 'wpsl_address2', m.meta_value, '')) AS sl_address2,
				MAX( IF(m.meta_key = 'wpsl_city', m.meta_value, '')) AS sl_city,
				MAX( IF(m.meta_key = 'wpsl_state', m.meta_value, '')) AS sl_state,
				MAX( IF(m.meta_key = 'wpsl_zip', m.meta_value, '')) AS sl_zip,
				MAX( IF(m.meta_key = 'wpsl_country', m.meta_value, '')) AS sl_country,
				MAX( IF(m.meta_key = 'wpsl_lat', m.meta_value, '')) AS sl_latitude,
				MAX( IF(m.meta_key = 'wpsl_lng', m.meta_value, '')) AS sl_longitude,
				REPLACE(p.post_content, '&amp;', '&') AS sl_description,
				t.name AS sl_tags,
				MAX( IF(m.meta_key = 'wpsl_url', m.meta_value, '')) AS sl_url,
				MAX( IF(m.meta_key = 'wpsl_phone', m.meta_value, '')) AS sl_phone
			FROM
				$wpdb->postmeta m
				INNER JOIN $wpdb->posts p ON m.post_id = p.id
				INNER JOIN $wpdb->term_relationships tr ON m.post_id = tr.object_id
				INNER JOIN $wpdb->term_taxonomy tx ON tr.term_taxonomy_id = tx.term_taxonomy_id
				INNER JOIN $wpdb->terms t ON tx.term_id = t.term_id
			WHERE
				p.post_type = 'wpsl_stores'
			GROUP BY
				m.post_id",
			ARRAY_N
		);

		// load the CSV document from a string.
		$csv = Writer::createFromString( '' );

		// insert the header.
		$csv->insertOne( $headers );

		// insert all the records.
		$csv->insertAll( $data );

		header( 'Content-Type: text/csv; charset=UTF-8' );
		header( 'Content-Description: File Transfer' );
		header( 'Content-Disposition: attachment; filename="' . $wpdb->posts . date( '-d-m-y' ) . '.csv"' );

		Encoding::toUTF8( Encoding::fixUTF8( $csv->output() ) );

		restore_current_blog();

		wp_die();

	} // export_wpsl

	/**
	 * Handle AJAX: Retrieve the requests post.
	 *
	 * @since 1.0.0
	 *
	 * @internal Only to be used by AJAX.
	 *
	 * @return void
	 */
	public function view_location_tool_log() {

		if ( ! current_user_can( 'manage_network' ) ) {
			wp_send_json_error( self::error_message( 'permission' ) );
		}

		// Security check.
		check_ajax_referer( 'epra_location_import_tool_log_view' );

		try {
			$response = get_post( ( isset( $_GET['postid'] ) ) ? (int) $_GET['postid'] : wp_send_json_error( self::error_message( 'no_postid' ) ) );
			$content = $response->post_content;
		} catch ( Exception $e ) {
			wp_send_json_error( $e->getMessage() );
		}

		// Send response in JSON format.
		echo wp_kses_post( $content );

		wp_die();

	} // view_location_tool_log

	/**
	 * Returns a predefined error message.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @param string $message Error message to retrieve.
	 * @return string
	 */
	private function error_message( string $message ) {

		/**
		 * Filter the available error messages for AJAX functions.
		 *
		 * @since 1.0.0
		 */
		$errors = apply_filters(
			'epra_location_import_tool_ajax_error_messages',
			array(
				'permission' => __( 'You do not have the correct permissions to do this.', 'epra-location-import-tool' ),
				'no_table'   => __( 'No origin table selected.', 'epra-location-import-tool' ),
				'table_null' => __( 'Selected table does not exist.', 'epra-location-import-tool' ),
				'no_blogid'  => __( 'No blog ID specified.', 'epra-location-import-tool' ),
				'no_postid'  => __( 'No post ID specified.', 'epra-location-import-tool' ),
			)
		);

		return $errors[ $message ];

	} // error_message

} // Ajax
