<?php
/**
 * Fired during plugin deactivation.
 *
 * @since 1.0.0
 *
 * @package    Epra_Location_Import_Tool
 * @subpackage Epra_Location_Import_Tool/includes
 */

namespace Epra_Location_Import_Tool\includes;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * This class defines all code necessary to run during the plugin's deactivation.
 */
class Deactivator {

	/**
	 * Run on deactivation.
	 *
	 * @since 1.0.1
	 *
	 * @internal
	 *
	 * @return void
	 */
	public static function deactivate() {

		// clear all crons.
		foreach ( get_sites() as $site ) {
			// Switch to each site and update the cron setting.
			switch_to_blog( $site->blog_id );

			$has_epra_location_cron = wp_get_schedule( 'epra_location_import_hook' );

			if ( false !== $has_epra_location_cron ) {
				wp_clear_scheduled_hook( 'epra_location_import_hook' );

				$timestamp = wp_next_scheduled( 'epra_location_import_hook' );
				wp_unschedule_event( $timestamp, 'epra_location_import_hook' );

				wp_clear_scheduled_hook( 'epra_location_import_hook' );
			}
		}

		restore_current_blog();

	} // deactivate

} // Epra_Location_Import_Tool_Deactivator
