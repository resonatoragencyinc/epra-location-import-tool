<?php
/**
 * The importing logic.

 * @since 1.0.0
 *
 * @package    Epra_Location_Import_Tool
 * @subpackage Epra_Location_Import_Tool/includes
 */

namespace Epra_Location_Import_Tool\includes;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

use \Epra_Location_Import_Tool\includes\Logger;
use \ForceUTF8\Encoding;

/**
 * This class defines the importing logic.
 */
class Importer {

	/**
	 * The output template.
	 *
	 * @since 1.0.0
	 *
	 * @var array
	 */
	private $output;

	/**
	 * Initialize the class.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		$this->output = array(
			'info'    => array(),
			'added'   => array(),
			'updated' => array(),
			'deleted' => array(),
			'error'   => array(),
		);

	}

	/**
	 * Run the importer and log the results.
	 *
	 * @since 1.0.0
	 *
	 * @param array $args {
	 *     An array of arguments.
	 *
	 *     @type int    blog_id
	 *     @type string table     Origin table name. Leave blank to skip.
	 *     @type bool   delete    Set true to remove all current locations before continuing. Default false.
	 *     @type string log_title Title of the log entry to be added. Default 'Log Entry'.
	 * }
	 * @return array
	 */
	public function epra_location_import( array $args ) {

		$defaults = array(
			'blog_id'   => '',
			'table'     => '',
			'delete'    => false,
			'log_title' => 'Log Entry',
		);

		$args = wp_parse_args( $args, $defaults );

		if ( empty( $args['blog_id'] ) ) {
			return new \WP_Error( 'no_blog_id', 'blog_id must be defined' );
		}

		// Remove any PHP time limitations so it does not timeout while running.
		set_time_limit( 0 );

		// Start timers to keep track of usage.
		$start_time = microtime( true );
		$ru_start   = getrusage();
		$output     = $this->output;

		if ( ! $args['delete'] && empty( $args['table'] ) ) {
			$output[]['info'][] = 'No action has been performed.';
		}

		if ( $args['delete'] ) {
			$output[] = $this->remove_locations( $args['blog_id'] );
		}

		if ( ! empty( $args['table'] ) ) {
			$output[] = $this->do_update( $args['blog_id'], $args['table'] );
		}

		$ru = getrusage();
		$output[]['info'][] = '<p>This process used ' . $this->ru_time( $ru, $ru_start, 'utime' ) .
			' ms for its computations<br>It spent ' . $this->ru_time( $ru, $ru_start, 'stime' ) .
			' ms in system calls<br>Completed in ' . ( microtime( true ) - $start_time ) . ' seconds</p>';

		$output = call_user_func_array( 'array_merge_recursive', $output );
		$output = $this->format_log( $output );

		$log_data = apply_filters(
			'epra_location_import_tool_log_data',
			array(
				'post_title'   => $args['log_title'],
				'post_content' => $output['message'],
				'log_type'     => 'event',
			)
		);
		$log_meta = apply_filters(
			'epra_location_import_tool_log_meta',
			array(
				'blog_id' => $args['blog_id'],
				'errors'  => $output['count']['error'],
				'added'   => $output['count']['added'],
				'updated' => $output['count']['updated'],
				'deleted' => $output['count']['deleted'],
			)
		);

		if ( defined( 'DOING_CRON' ) && DOING_CRON ) {
			$log_meta['run_by_cron'] = 'true';
		}

		// only write to primary network site.
		Logger::insert_network_log( $log_data, $log_meta );

		return $output;

	} // epra_location_import

	/**
	 * Handle Cron: Function to be run by each site's cron.
	 *
	 * @since 1.0.1
	 *
	 * @internal Do not run on its own as side-effects are unknown. Hooked into the WP cron system.
	 *
	 * @return void
	 */
	public function epra_location_import_cron() {

		$blogid = get_current_blog_id();

		$args = array(
			'blog_id'   => $blogid,
			'table'     => get_site_option( 'epra_location_import_tool' )[ 'origin_table_' . $blogid ],
			'delete'    => false,
			'log_title' => apply_filters( 'epra_location_import_tool_log_title', 'WP Cron - Compare' ),
		);

		if ( '1' === get_site_option( 'epra_location_import_tool' )[ 'enable_cron_' . $blogid ] ) {
			$this->epra_location_import( $args );
		}

	} // epra_location_import_cron

	/**
	 * Compare the locations in the WP posts table to the locations in the supplied table.
	 *
	 * @since 1.0.0
	 * @global object $wpdb
	 *
	 * @internal Used by self::do_update().
	 *
	 * @param string $table Name of origin table.
	 * @return array $differences An array of objects.
	 */
	private function get_differences( string $table ) {

		global $wpdb;

		if ( $table !== $wpdb->get_var( $wpdb->prepare( 'SHOW TABLES LIKE %s', $table ) ) ) {
			return new \WP_Error(
				'noorigintable',
				sprintf(
					// translators: placeholder will be the name of a database table.
					__( "The table '%s' does not exist.", 'epra-location-import-tool' ),
					$table
				)
			);
		}

		// phpcs:disable
		// {$table} is throwing a false positive
		$differences = $wpdb->get_results(
			/**
			 * Filters the SQL to look for differences.
			 *
			 * @since 1.0.3
			 */
			apply_filters(
				'epra_location_import_tool_differences_sql',
				"SELECT * FROM
				(SELECT
					CONVERT(MAX( IF(m.meta_key = 'wpsl_id', m.meta_value, '')), UNSIGNED INTEGER) AS sl_id,
					REPLACE(p.post_title, '&amp;', '&') AS sl_store,
					MAX( IF(m.meta_key = 'wpsl_address', m.meta_value, '')) AS sl_address,
					MAX( IF(m.meta_key = 'wpsl_address2', m.meta_value, '')) AS sl_address2,
					MAX( IF(m.meta_key = 'wpsl_city', m.meta_value, '')) AS sl_city,
					MAX( IF(m.meta_key = 'wpsl_state', m.meta_value, '')) AS sl_state,
					MAX( IF(m.meta_key = 'wpsl_zip', m.meta_value, '')) AS sl_zip,
					MAX( IF(m.meta_key = 'wpsl_country', m.meta_value, '')) AS sl_country,
					MAX( IF(m.meta_key = 'wpsl_lat', m.meta_value, '')) AS sl_latitude,
					MAX( IF(m.meta_key = 'wpsl_lng', m.meta_value, '')) AS sl_longitude,
					REPLACE(p.post_content, '&amp;', '&') AS sl_description,
					t.name AS sl_tags,
					MAX( IF(m.meta_key = 'wpsl_url', m.meta_value, '')) AS sl_url,
					MAX( IF(m.meta_key = 'wpsl_phone', m.meta_value, '')) AS sl_phone,
					'$wpdb->posts' AS source
				FROM
					$wpdb->postmeta m
					INNER JOIN $wpdb->posts p ON m.post_id = p.id
					INNER JOIN $wpdb->term_relationships tr ON m.post_id = tr.object_id
					INNER JOIN $wpdb->term_taxonomy tx ON tr.term_taxonomy_id = tx.term_taxonomy_id
					INNER JOIN $wpdb->terms t ON tx.term_id = t.term_id
				WHERE
					p.post_type = 'wpsl_stores'
				GROUP BY
					m.post_id
				UNION ALL
				SELECT
					CONVERT(loc.sl_id, UNSIGNED INTEGER),
					loc.sl_store,
					loc.sl_address,
					loc.sl_address2,
					loc.sl_city,
					loc.sl_state,
					loc.sl_zip,
					loc.sl_country,
					loc.sl_latitude,
					loc.sl_longitude,
					CONCAT_WS(CHAR(10 USING utf8), NULLIF(loc.sl_hours, ' '), NULLIF(loc.sl_description, ' ')) AS sl_description,
					loc.sl_tags,
					loc.sl_url,
					loc.sl_phone,
					'$table' AS source
				FROM
					$table loc
				WHERE
					loc.sl_latitude IS NOT NULL AND loc.sl_latitude != '') t
				GROUP BY 
					sl_id,
					sl_store,
					sl_address,
					sl_address2,
					sl_city,
					sl_state,
					sl_zip,
					sl_country,
					sl_latitude,
					sl_longitude,
					sl_description,
					sl_tags,
					sl_url,
					sl_phone
				HAVING
					COUNT(*) = 1
				ORDER BY
					sl_id"
			),
			'OBJECT'
		);
		// phpcs:enable

		return $differences;

	} // get_differences

	/**
	 * Delete all posts with post_type 'wpsl_stores'
	 *
	 * @since 1.0.0
	 * @global object $wpdb
	 *
	 * @internal Should always be called through self::epra_location_import().
	 *
	 * @param int $blog_id ID of blog to remove posts from.
	 * @return array An array of output messages.
	 */
	private function remove_locations( int $blog_id = null ) {

		global $wpdb;

		$output = $this->output;

		// Switch to the correct blog before doing anything else.
		switch_to_blog( $blog_id );

		// phpcs:disable
		// apply_filters is throwing a false positive
		$output['deleted']['rows'] = $wpdb->query(
			/**
			 * Filters the SQL to delete locations.
			 *
			 * @since 1.0.3
			 */
			apply_filters(
				'epra_location_import_tool_delete_locations_sql',
				"DELETE a,b,c
				FROM $wpdb->posts a
				LEFT JOIN $wpdb->term_relationships b
					ON (a.ID = b.object_id)
				LEFT JOIN $wpdb->postmeta c
					ON (a.ID = c.post_id)
				WHERE a.post_type = 'wpsl_stores'"
			)
		);
		// phpcs:enable

		$terms = get_terms( 'wpsl_store_category' );
		$termtax = array();

		if ( is_wp_error( $terms ) ) {
			$output['error'][] = 'Failed to find taxonomy.<br>' . print_r( $terms, true );
		} else {
			foreach ( $terms as $term ) {
				$termtax[] = $term->term_taxonomy_id;
			}

			wp_update_term_count( $termtax, 'wpsl_store_category' );
		}

		$this->delete_wpsl_transients();

		// Switch back to the main site (ID 1) as a precaution.
		restore_current_blog();

		return $output;

	} // remove_locations

	/**
	 * Stolen directly from the WPSL code to clear transients.
	 *
	 * @since 1.0.0
	 * @global object $wpdb
	 *
	 * @internal Only to be run after making updates/deletions using self::epra_location_import().
	 *
	 * @return void
	 */
	private function delete_wpsl_transients() {

		global $wpdb;

		$option_names = $wpdb->get_results( 'SELECT option_name AS transient_name FROM ' . $wpdb->options . " WHERE option_name LIKE ('\_transient\_wpsl\_autoload\_%')" );

		if ( $option_names ) {
			foreach ( $option_names as $option_name ) {
				$transient_name = str_replace( '_transient_', '', $option_name->transient_name );

				delete_transient( $transient_name );
			}
		}

	} // delete_wpsl_transients

	/**
	 * Get differences and make necessary changes.
	 *
	 * @since 1.0.0
	 * @global object $wpdb
	 *
	 * @internal Should always be called through self::epra_location_import().
	 *
	 * @param int    $blog_id        ID the blog to update.
	 * @param string $location_table Name of location origin table.
	 * @return array An array of output messages.
	 */
	private function do_update( int $blog_id, string $location_table ) {

		global $wpdb;

		$output = $this->output;

		// Switch to the correct blog before doing anything else.
		switch_to_blog( $blog_id );

		$tables = array(
			"$wpdb->posts" => array(),
			"{$location_table}" => array(),
		);

		/**
		 * Filters the differences between the blog's posts table and location origin table.
		 *
		 * @since 1.0.3
		 *
		 * @param object $diffs Object containing all the differences between the two tables.
		 * @param int    $post  The blog's ID.
		 */
		$diffs = apply_filters( 'epra_location_import_tool_update_differences', $this->get_differences( $location_table ), $blog_id );

		if ( is_wp_error( $diffs ) ) {
			$output['error'][] = 'No origin table is selected or the selected table does not exist.';
			return $output;
		}

		if ( empty( $diffs ) ) {
			$output['info'][] = 'No updates to make.';
			return $output;
		}

		// Separate out the different sources into their own sub array.
		foreach ( $diffs as $key => $object ) {
			$tables[ $object->source ][ $object->sl_id ] = $object;
		}

		/**
		 * Fires before any post manipulation.
		 *
		 * @since 1.0.3
		 *
		 * @param array $tables An array containing all the changes, seperated into two nested arrays of the tweo tables compared.
		 */
		do_action( 'before_epra_location_import_tool_do_update', $tables );

		// Remove the function 'do_all_pings' - this is to stop taking up resources with pingbacks,
		// enclosures, trackbacks, and sending to pingback services.
		remove_all_actions( 'do_pings' );
		remove_all_actions( 'save_post' );

		// Perform all the necessary changes before writing to the database. The two defer functions saves from having to do
		// extra work to index posts each time one is added, until all the posts are added
		// For more information see: https://wordpress.stackexchange.com/questions/219975/is-there-a-downside-of-using-wp-defer-term-counting.
		wp_defer_term_counting( true );
		wp_defer_comment_counting( true );

		foreach ( $tables[ "$wpdb->posts" ] as $key => $location ) {
			$post = new \WP_Query(
				array(
					'no_found_rows'          => true,
					'update_post_meta_cache' => false,
					'update_post_term_cache' => false,
					'post_type'              => 'wpsl_stores',
					'post_status'            => 'publish',
					'meta_key'               => 'wpsl_id',
					'meta_value'             => $key,
					'fields'                 => 'ids',
					'post_count'             => 1,
				)
			);

			if ( ! $post->posts ) {
				$output['error'][] = "No post can be found for ID $key - $location->sl_store.";
			}

			$update = array_key_exists( $key, $tables[ $location_table ] );

			/**
			 * Fires before loction is updated or deleted.
			 *
			 * @since 1.0.3
			 *
			 * @param string  $key      The ID of the location.
			 * @param array   $location An associative array of the location's information.
			 * @param WP_Post $post     The WP_post object found by WP_Query.
			 * @param bool    $update   True is location is to be updated, false if to be added.
			 */
			do_action( 'before_epra_location_import_tool_single_location_update', $key, $location, $post, $update );

			if ( $update ) {
				$id          = Encoding::toUTF8( $tables[ $location_table ][ $key ]->sl_id );
				$store       = Encoding::toUTF8( $tables[ $location_table ][ $key ]->sl_store );
				$cat         = Encoding::toUTF8( $tables[ $location_table ][ $key ]->sl_tags );
				$address     = Encoding::toUTF8( $tables[ $location_table ][ $key ]->sl_address );
				$address2    = Encoding::toUTF8( $tables[ $location_table ][ $key ]->sl_address2 );
				$city        = Encoding::toUTF8( $tables[ $location_table ][ $key ]->sl_city );
				$province    = Encoding::toUTF8( $tables[ $location_table ][ $key ]->sl_state );
				$postal      = Encoding::toUTF8( $tables[ $location_table ][ $key ]->sl_zip );
				$phone       = Encoding::toUTF8( $tables[ $location_table ][ $key ]->sl_phone );
				$description = Encoding::toUTF8( $tables[ $location_table ][ $key ]->sl_description );
				$url         = Encoding::toUTF8( $tables[ $location_table ][ $key ]->sl_url );

				// Check for protocol, added https if none found.
				if ( ! empty( $url ) && ! preg_match( '~^(?:f|ht)tps?://~i', $url ) ) {
					$url = 'https://' . $url;
				}

				$lat = $tables[ $location_table ][ $key ]->sl_latitude;
				$lng = $tables[ $location_table ][ $key ]->sl_longitude;

				// Create post object for the location.
				$updated_location = array(
					'ID'           => $post->posts[0],
					'post_title'   => $store,
					// Put hours and description into one string, seperated from an end of line character.
					'post_content' => $description,
					'meta_input'   => array(
						'wpsl_address'  => $address,
						'wpsl_address2' => $address2,
						'wpsl_city'     => $city,
						'wpsl_state'    => $province,
						'wpsl_zip'      => $postal,
						'wpsl_phone'    => $phone,
						'wpsl_url'      => $url,
						'wpsl_lat'      => $lat,
						'wpsl_lng'      => $lng,
					),
				);

				$updating          = wp_update_post( $updated_location, true );
				$term_taxonomy_ids = wp_set_object_terms( $post->posts[0], $cat, 'wpsl_store_category' );

				if ( is_wp_error( $updating ) || is_wp_error( $term_taxonomy_ids ) ) {
					$output['error'][] = "Failed to update Post ID: {$post->posts[0]} - $id - $store.<br>" .
						print_r( $updating, true ) .
						print_r( $term_taxonomy_ids, true );
				}

				$output['updated'][] = "<p>Post ID: {$post->posts} - {$updated_location['post_title']}<br>" .
					"ID: {$id}<br>" .
					"Description: {$updated_location['post_content']}<br>" .
					"Address: {$updated_location['meta_input']['wpsl_address']}<br>" .
					"City: {$updated_location['meta_input']['wpsl_city']}<br>" .
					"Province: {$updated_location['meta_input']['wpsl_state']}<br>" .
					"Postal Code: {$updated_location['meta_input']['wpsl_zip']}<br>" .
					"Phone: {$updated_location['meta_input']['wpsl_phone']}<br>" .
					"URL: {$updated_location['meta_input']['wpsl_url']}<br>" .
					"Latitude: {$updated_location['meta_input']['wpsl_lat']}<br>" .
					"Longitude: {$updated_location['meta_input']['wpsl_lng']}<br>" .
					"Category: {$cat}<p>";

				/**
				 * Fires after location is updated.
				 *
				 * @since 1.0.3
				 *
				 * @param string       $key      The ID of the location.
				 * @param array        $location An associative array of the location's information.
				 * @param int|WP_Error $updating The post ID if post created, else WP_Error.
				 * @param array        $output   An associative array containing the log messages.
				 */
				do_action( 'after_epra_location_import_tool_single_location_update', $key, $location, $updating, $output );
			} elseif ( ! $update && is_numeric( $post->posts[0] ) ) {
				$deleted             = wp_delete_post( $post->posts[0] );

				if ( ! $deleted ) {
					$output['error'][] = "<p>Something went wrong while deleting {$post->posts[0]} - $key - $deleted->post_title.</p>";
				} else {
					$output['deleted'][] = "<p>Post ID: $deleted->ID - $key - $deleted->post_title.</p>";
				}

				/**
				 * Fires after location is deleted.
				 *
				 * @since 1.0.3
				 *
				 * @param string       $key      The ID of the location.
				 * @param array        $location An associative array of the location's information.
				 * @param WP_Post|bool $deleted  The post object if post deleted, else false.
				 * @param array        $output   An associative array containing the log messages.
				 */
				do_action( 'after_epra_location_import_tool_single_location_delete', $key, $location, $deleted, $output );
			}
		}

		foreach ( $tables[ $location_table ] as $key => $location ) {
			$exists = array_key_exists( $key, $tables[ "$wpdb->posts" ] );

			if ( ! $exists ) {
				$id          = Encoding::toUTF8( $tables[ $location_table ][ $key ]->sl_id );
				$store       = Encoding::toUTF8( $tables[ $location_table ][ $key ]->sl_store );
				$cat         = Encoding::toUTF8( $tables[ $location_table ][ $key ]->sl_tags );
				$address     = Encoding::toUTF8( $tables[ $location_table ][ $key ]->sl_address );
				$address2    = Encoding::toUTF8( $tables[ $location_table ][ $key ]->sl_address2 );
				$city        = Encoding::toUTF8( $tables[ $location_table ][ $key ]->sl_city );
				$province    = Encoding::toUTF8( $tables[ $location_table ][ $key ]->sl_state );
				$postal      = Encoding::toUTF8( $tables[ $location_table ][ $key ]->sl_zip );
				$phone       = Encoding::toUTF8( $tables[ $location_table ][ $key ]->sl_phone );
				$description = Encoding::toUTF8( $tables[ $location_table ][ $key ]->sl_description );
				$url         = Encoding::toUTF8( $tables[ $location_table ][ $key ]->sl_url );

				// Check for protocol, added https if none found.
				if ( ! empty( $url ) && ! preg_match( '~^(?:f|ht)tps?://~i', $url ) ) {
					$url = 'https://' . $url;
				}

				$lat = $tables[ $location_table ][ $key ]->sl_latitude;
				$lng = $tables[ $location_table ][ $key ]->sl_longitude;

				// Fail location is lat or long is missing and make a note of it.
				if ( empty( $lat ) || empty( $lng ) ) {
					$output['error'][] = "$id - $store - Latitude and/or Longitude is missing. Location will not be added.<br>";
					continue;
				}

				// Create post object for the location.
				$new_location = array(
					'post_type'      => 'wpsl_stores',
					'post_title'     => $store,
					// Put hours and description into one string, seperated from an end of line character.
					'post_content'   => $description,
					'post_status'    => 'publish',
					'post_author'    => '1',
					'comment_status' => 'closed',
					'ping_status'    => 'closed',
					'meta_input'     => array(
						'wpsl_id'         => $id,
						'wpsl_featured' => '',
						'wpsl_address'  => $address,
						'wpsl_address2' => $address2,
						'wpsl_city'     => $city,
						'wpsl_state'    => $province,
						'wpsl_zip'      => $postal,
						'wpsl_country'  => 'Canada',
						'wpsl_phone'    => $phone,
						'wpsl_email'    => '',
						'wpsl_fax'      => '',
						'wpsl_url'      => $url,
						'wpsl_lat'      => $lat,
						'wpsl_lng'      => $lng,
					),
				);

				/**
				 * Fires before location is added.
				 *
				 * @since 1.0.3
				 *
				 * @param string $key          The ID of the location.
				 * @param array  $location     An associative array of the location's information.
				 * @param array  $new_location An associative array of elements that make up a post to update or insert.
				 */
				do_action( 'before_epra_location_import_tool_single_location_add', $key, $location, $new_location );

				// Insert the post into the database.
				$post_id = wp_insert_post( $new_location, true );

				// Fail if wp_insert_post throws and error.
				if ( is_wp_error( $post_id ) ) {
					$output['error'][] = "Failed to add {$new_location['meta_input']['wpsl_id']} - {$new_location['post_title']}.<br>" . print_r( $post_id, true );
				}

				// Append all the information for the location added to the results e-mail.
				$output['added'][] = "<p>Post ID: {$post_id} - {$new_location['post_title']}<br>" .
					"ID: {$new_location['meta_input']['wpsl_id']}<br>" .
					"Description: {$new_location['post_content']}<br>" .
					"Address: {$new_location['meta_input']['wpsl_address']}<br>" .
					"City: {$new_location['meta_input']['wpsl_city']}<br>" .
					"Province: {$new_location['meta_input']['wpsl_state']}<br>" .
					"Postal Code: {$new_location['meta_input']['wpsl_zip']}<br>" .
					"Phone: {$new_location['meta_input']['wpsl_phone']}<br>" .
					"URL: {$new_location['meta_input']['wpsl_url']}<br>" .
					"Latitude: {$new_location['meta_input']['wpsl_lat']}<br>" .
					"Longitude: {$new_location['meta_input']['wpsl_lng']}</p>";

				// Set the proper category.
				$term_taxonomy_ids = wp_set_object_terms( $post_id, $cat, 'wpsl_store_category' );

				if ( is_wp_error( $term_taxonomy_ids ) ) {
					$output['error'][] = "Failed to add category for $post_id - $store.<br>" . print_r( $term_taxonomy_ids, true );
				}

				/**
				 * Fires after location is added.
				 *
				 * @since 1.0.3
				 *
				 * @param string       $key          The ID of the location.
				 * @param array        $location     An associative array of the location's information.
				 * @param array        $new_location An associative array of elements that make up a post to update or insert.
				 * @param int|WP_Error $post_id      The post ID if post created, else WP_Error.
				 * @param array        $output       An associative array containing the log messages.
				 */
				do_action( 'after_epra_location_import_tool_single_location_add', $key, $location, $new_location, $post_id, $output );
			}
		}

		// Count the posts now to have the correct values for terms.
		wp_defer_term_counting( false );
		wp_defer_comment_counting( false );

		// Clear the WP transients for WPSL to ensure it rebuilds all the location data.
		$this->delete_wpsl_transients();

		/**
		 * Fires after any post manipulation.
		 *
		 * @since 1.0.3
		 *
		 * @param array $tables An array containing all the changes, seperated into two nested arrays of the tweo tables compared.
		 */
		do_action( 'after_epra_location_import_tool_do_update', $tables );

		// Switch back to the main site (ID 1) as a precaution.
		restore_current_blog();

		return $output;

	} // do_update

	/**
	 * I'm actually not sure what this is doing. It just does.
	 *
	 * @since 1.0.0
	 *
	 * @internal Only used to calculate process and computation time for the log output.
	 *
	 * @param array  $ru    Ending resource usages from getrusage().
	 * @param array  $rus   Starting resource usages from getrusage().
	 * @param string $index utime or stime. ru_$index.tv_sec.
	 * @return string
	 */
	private function ru_time( array $ru, array $rus, string $index ) {

		return ( $ru[ "ru_$index.tv_sec" ] * 1000 + intval( $ru[ "ru_$index.tv_usec" ] / 1000 ) )
		- ( $rus[ "ru_$index.tv_sec" ] * 1000 + intval( $rus[ "ru_$index.tv_usec" ] / 1000 ) );

	} // ru_time

	/**
	 * Formats the log data.
	 *
	 * Formats the log data array into a messages section, with each type as headers
	 * , and a count for each type.
	 *
	 * @since 1.0.1
	 *
	 * @internal Expects a very particular input so shouldn't be used outisde of this class.
	 *
	 * @param array $data Unformatted log data.
	 * @return array Formatted output of log and count of each log type.
	 */
	private function format_log( array $data ) {

		$log = array(
			'count'   => array(),
			'message' => '',
		);

		foreach ( $data as $key => $value ) {
			$log['count'][ $key ] = count( $value );

			if ( empty( $log['count'][ $key ] ) ) {
				continue;
			}

			$log['message'] .= '<h2>' . ucwords( $key ) . '</h2>';

			foreach ( $value as $vkey => $text ) {
				if ( 'rows' === $vkey ) {
					$log['message'] .= "<p>Rows removed during DELETE query $text</p>";
					continue;
				}

				$log['message'] .= $text;
			}
		}

		return $log;

	} // format_log

} // Epra_Location_Import_Tool_Importer
