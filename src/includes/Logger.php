<?php
/**
 * Log events and errors.
 *
 * @since 1.0.0
 *
 * @package    Epra_Location_Import_Tool
 * @subpackage Epra_Location_Import_Tool/includes
 *
 * @author  Pippin Williamson
 * @license http://opensource.org/licenses/gpl-2.0.php GNU Public License
 */

namespace Epra_Location_Import_Tool\includes;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class for logging events and errors.
 */
class Logger {

	/**
	 * Class constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		// create the log post type.
		add_action( 'init', array( $this, 'register_post_type' ) );

		// create types taxonomy and default types.
		add_action( 'init', array( $this, 'register_taxonomy' ) );

		// make a cron job for this hook to start pruning.
		add_action( 'epra_location_import_tool_logger_prune_routine', array( $this, 'prune_logs' ) );

	}

	/**
	 * Allows you to tie in a cron job and prune old logs.
	 *
	 * @since 1.0.0
	 *
	 * @internal Only to be used by cron.
	 *
	 * @return void
	 */
	public function prune_logs() {

		$should_we_prune = apply_filters( 'epra_location_import_tool_logger_should_we_prune', false );

		if ( false === $should_we_prune ) {
			return;
		}

		$logs_to_prune = $this->get_logs_to_prune();

		if ( isset( $logs_to_prune ) && ! empty( $logs_to_prune ) ) {
			$this->prune_old_logs( $logs_to_prune );
		}

	} // prune_logs

	/**
	 * Deletes the old logs that we don't want
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @param array $logs The array of WP_Post objects we want to prune.
	 * @return void
	 */
	private function prune_old_logs( array $logs ) {

		/**
		 * Allows user to override the force delete setting which bypasses the trash by setting to false.
		 *
		 * @since 1.0.0
		 *
		 * @param bool
		 */
		$force = apply_filters( 'epra_location_import_tool_logger_force_delete_log', true );

		foreach ( $logs as $l ) {
			$id = is_int( $l ) ? $l : $l->ID;
			wp_delete_post( $id, $force );
		}

	} // prune_old_logs

	/**
	 * Returns an array of posts that are prune candidates.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @return array $old_logs The array of posts that was returned from get_posts.
	 */
	private function get_logs_to_prune() {

		/**
		 * Users can change how long ago we are looking for logs to prune.
		 *
		 * @since 1.0.0
		 *
		 * @param string|array $args {
		 *     Accepts strtotime()-compatible string, or array of ‘year’, ‘month’, ‘day’ values.
		 *
		 *     @type string year  Accepts any four-digit year.
		 *     @type string month The month of the year. Accepts numbers 1-12.
		 *     @type string day   The day of the month. Accepts numbers 1-31.
		 * }
		 */
		$how_old = apply_filters( 'epra_location_import_tool_logger_prune_when', '2 weeks ago' );

		$args = array(
			'post_type'      => 'location_tool_log',
			'posts_per_page' => '100',
			'date_query'     => array(
				array(
					'column' => 'post_date_gmt',
					'before' => (string) $how_old,
				),
			),
		);

		/**
		 * Filters the query args for pruning.
		 *
		 * @since 1.0.0
		 *
		 * @param array $args An array of args for get_posts(). {@see https://developer.wordpress.org/reference/classes/WP_Query/parse_query/}
		 */
		$old_logs = get_posts( apply_filters( 'epra_location_import_tool_logger_prune_query_args', $args ) );

		return $old_logs;

	} // get_logs_to_prune

	/**
	 * Log types.
	 *
	 * Sets up the default log types and allows for new ones to be created.
	 *
	 * @since 1.0.0
	 *
	 * @internal
	 *
	 * @return array
	 */
	private static function log_types() {
		$terms = array(
			'error',
			'event',
		);

		/**
		 * Filters the array of log types to be used.
		 *
		 * @since 1.0.0
		 *
		 * @param array $terms Array of the existing terms.
		 */
		return apply_filters( 'epra_location_import_tool_logger_log_types', $terms );
	} // log_types


	/**
	 * Registers the location_tool_log Post Type
	 *
	 * @since  1.0.0
	 *
	 * @internal Used by register post type hook.
	 *
	 * @return void
	 */
	public function register_post_type() {

		/* logs post type */

		$log_args = array(
			'labels'          => array( 'name' => __( 'Logs', 'epra-location-import-tool' ) ),
			'public'          => defined( 'WP_DEBUG' ) && WP_DEBUG,
			'query_var'       => false,
			'rewrite'         => false,
			'capability_type' => 'post',
			'supports'        => array( 'title', 'editor' ),
			'can_export'      => false,
		);

		/**
		 * Filters the args for register_post_type.
		 *
		 * @since 1.0.0
		 *
		 * @param array $log_args
		 */
		register_post_type( 'location_tool_log', apply_filters( 'epra_location_import_tool_logger_post_type_args', $log_args ) );

	} // register_post_type


	/**
	 * Registers the Type Taxonomy
	 *
	 * The Type taxonomy is used to determine the type of log entry
	 *
	 * @since  1.0
	 *
	 * @internal Used by register taxonomy hook.
	 *
	 * @return void
	 */
	public function register_taxonomy() {

		register_taxonomy(
			'location_tool_log_type',
			'location_tool_log',
			array( 'public' => defined( 'WP_DEBUG' ) && WP_DEBUG )
		);

		$types = self::log_types();

		foreach ( $types as $type ) {
			if ( ! term_exists( $type, 'location_tool_log_type' ) ) {
				wp_insert_term( $type, 'location_tool_log_type' );
			}
		}

	} // register_taxonomy


	/**
	 * Check if a log type is valid
	 *
	 * Checks to see if the specified type is in the registered list of types
	 *
	 * @since  1.0
	 *
	 * @internal Only to be used by this class.
	 *
	 * @param string $type The log type.
	 * @return bool True if log type found, false otherwise.
	 */
	private static function valid_type( string $type ) {
		return in_array( $type, self::log_types() );
	} // valid_type


	/**
	 * Create new log entry.
	 *
	 * This is just a simple and fast way to log something. Use self::insert_log()
	 * if you need to store custom meta data.
	 *
	 * @since 1.0.0
	 *
	 * @param string $title   Optional. The log's title.
	 * @param string $message Optional. The log's content.
	 * @param int    $parent  Optional. The log's parent ID.
	 * @param string $type    Optional. The log's type.
	 * @return int The ID of the new log entry
	 */
	public static function add( string $title = '', string $message = '', int $parent = 0, string $type = null ) {

		$log_data = array(
			'post_title'   => $title,
			'post_content' => $message,
			'post_parent'  => $parent,
			'log_type'     => $type,
		);

		return self::insert_log( $log_data );

	} // add

	/**
	 * Create a new log entry on the primary site of a multisite.
	 *
	 * This is just a simple and fast way to log something to the primary network site.
	 * Use self::insert_log() or self::add() if you do not want to only add to primary site.
	 *
	 * @since 1.0.3
	 *
	 * @param array $log_data Optional. Arguments for wp_insert_post(). {@see https://developer.wordpress.org/reference/functions/wp_insert_post/}.
	 * @param array $log_meta Optional. Array of post meta.
	 * @return int|bool Post ID on success, flase on faliure.
	 */
	public static function insert_network_log( array $log_data = array(), array $log_meta = array() ) {

		switch_to_blog( get_network()->site_id );

		$log_id = self::insert_log( $log_data, $log_meta );

		restore_current_blog();

		return $log_id;

	} // insert_network_log


	/**
	 * Stores a log entry
	 *
	 * @since 1.0.0
	 *
	 * @param array $log_data Optional. Arguments for wp_insert_post(). {@see https://developer.wordpress.org/reference/functions/wp_insert_post/}.
	 * @param array $log_meta Optional. Array of post meta.
	 * @return int|bool Post ID on success, flase on faliure.
	 */
	public static function insert_log( array $log_data = array(), array $log_meta = array() ) {

		$defaults = array(
			'post_type'    => 'location_tool_log',
			'post_status'  => 'publish',
			'post_parent'  => 0,
			'post_content' => '',
			'log_type'     => false,
		);

		$args = wp_parse_args( $log_data, $defaults );

		/**
		 * Actions to run before log entry is created.
		 *
		 * @since 1.0.0
		 */
		do_action( 'location_tool_log_pre_insert' );

		// store the log entry.
		$log_id = wp_insert_post( $args );

		// set the log type, if any.
		if ( $log_data['log_type'] && self::valid_type( $log_data['log_type'] ) ) {
			wp_set_object_terms( $log_id, $log_data['log_type'], 'location_tool_log_type', false );
		}

		// set log meta, if any.
		if ( $log_id && ! empty( $log_meta ) ) {
			foreach ( (array) $log_meta as $key => $meta ) {
				update_post_meta( $log_id, '_location_tool_log_' . sanitize_key( $key ), $meta );
			}
		}

		/**
		 * Actions to run after log entry is created.
		 *
		 * @since 1.0.0
		 *
		 * @param string $log_id The ID of the post, if successful.
		 */
		do_action( 'location_tool_log_post_insert', $log_id );

		return $log_id;

	} // insert_log


	/**
	 * Update an existing log item
	 *
	 * @since 1.0.0
	 *
	 * @param array $log_data Optional. Arguments for wp_insert_post(). {@see https://developer.wordpress.org/reference/functions/wp_insert_post/}.
	 * @param array $log_meta Optional. Array of post meta.
	 * @return bool True if successful, false otherwise.
	 */
	public static function update_log( array $log_data = array(), array $log_meta = array() ) {

		$defaults = array(
			'post_type'   => 'location_tool_log',
			'post_status' => 'publish',
			'post_parent' => 0,
		);

		$args = wp_parse_args( $log_data, $defaults );

		/**
		 * Actions to run before log entry is updated.
		 *
		 * @since 1.0.0
		 */
		do_action( 'location_tool_log_pre_update' );

		// store the log entry.
		$log_id = wp_update_post( $args );

		if ( $log_id && ! empty( $log_meta ) ) {
			foreach ( (array) $log_meta as $key => $meta ) {
				if ( ! empty( $meta ) ) {
					update_post_meta( $log_id, '_location_tool_log_' . sanitize_key( $key ), $meta );
				}
			}
		}

		/**
		 * Actions to run after log entry is updated.
		 *
		 * @since 1.0.0
		 *
		 * @param string $log_id The ID of the post, if successful.
		 */
		do_action( 'location_tool_log_post_update', $log_id );

		return $log_id;

	} // update_log


	/**
	 * Easily retrieves log items for a particular object ID
	 *
	 * @since 1.0.0
	 *
	 * @param int    $object_id Optional. The posts' parent ID.
	 * @param string $type      Optional. The log type.
	 * @param int    $paged     Optional. The number of the current page.
	 * @return array|bool Array of WP_Post objects. False is no posts are found.
	 */
	public static function get_logs( int $object_id = 0, string $type = null, int $paged = null ) {

		return self::get_connected_logs(
			array(
				'post_parent' => $object_id,
				'paged' => $paged,
				'log_type' => $type,
			)
		);

	} // get_logs


	/**
	 * Retrieve all connected logs
	 *
	 * Used for retrieving logs related to particular items, such as a specific purchase.
	 *
	 * @since 1.0.0
	 *
	 * @param array $args Optional. Arguments for get_posts(). {@see https://developer.wordpress.org/reference/classes/wp_query/parse_query/}.
	 * @return array|bool Array of WP_Post objects. False is no posts are found.
	 */
	public static function get_connected_logs( array $args = array() ) {

		$defaults = array(
			'post_parent'    => 0,
			'post_type'      => 'location_tool_log',
			'posts_per_page' => 10,
			'post_status'    => 'publish',
			'paged'          => get_query_var( 'paged' ),
			'log_type'       => false,
		);

		$query_args = wp_parse_args( $args, $defaults );

		if ( $query_args['log_type'] && self::valid_type( $query_args['log_type'] ) ) {

			$query_args['tax_query'] = array(
				array(
					'taxonomy' => 'location_tool_log_type',
					'field'    => 'slug',
					'terms'    => $query_args['log_type'],
				),
			);

		}

		$logs = get_posts( $query_args );

		if ( $logs ) {
			return $logs;
		}

		// no logs found.
		return false;

	} // get_connected_logs

	/**
	 * Retrieves number of log entries connected to particular object ID
	 *
	 * @since 1.0.0
	 *
	 * @param int    $object_id  Optional. The posts' parent ID.
	 * @param string $type       Optional. The log type.
	 * @param array  $meta_query Optional. An associative array of WP_Meta_Query arguments. {@see https://developer.wordpress.org/reference/classes/wp_meta_query/}.
	 * @return int Number of posts found.
	 */
	public static function get_log_count( int $object_id = 0, string $type = null, array $meta_query = null ) {

		$query_args = array(
			'post_parent'    => $object_id,
			'post_type'      => 'location_tool_log',
			'posts_per_page' => -1,
			'post_status'    => 'publish',
		);

		if ( ! empty( $type ) && self::valid_type( $type ) ) {

			$query_args['tax_query'] = array(
				array(
					'taxonomy' => 'location_tool_log_type',
					'field'    => 'slug',
					'terms'    => $type,
				),
			);

		}

		if ( ! empty( $meta_query ) ) {
			$query_args['meta_query'] = $meta_query;
		}

		$logs = new \WP_Query( $query_args );

		return (int) $logs->post_count;

	} // get_log_count

} // Epra_Location_Import_Tool_Logger
