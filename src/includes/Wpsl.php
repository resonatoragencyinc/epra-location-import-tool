<?php
/**
 * The WPSL functionality of the plugin.
 *
 * @since 1.0.0
 *
 * @package    Epra_Location_Import_Tool
 * @subpackage Epra_Location_Import_Tool/admin
 */

namespace Epra_Location_Import_Tool\includes;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Defines the methods to change default WPSL behaviour.
 */
class Wpsl {

	/**
	 * Initialize the class
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

	}

	/**
	 * Add the Store ID meta field to WPSL posts.
	 *
	 * @since 1.0.1
	 *
	 * @internal Only to be used by WPSL post meta hook.
	 *
	 * @param array $meta_fields Associative array of existing meta fields.
	 * @return array Associative array of existing meta fields.
	 */
	public function custom_meta_box_fields( $meta_fields ) {

		$meta_fields[ __( 'Extra', 'epra-location-import-tool' ) ] = array(
			'id' => array(
				'label'    => __( 'Store ID', 'epra-location-import-tool' ),
				'required' => true, // This will place a * after the label.
			),
		);

		return $meta_fields;

	}
}
